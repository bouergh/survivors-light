Event	ID	Name			Wwise Object Path	Notes
	1131475472	Play_Diva_Laser_A			\Default Work Unit\Play_Diva_Laser_A	
	1131475475	Play_Diva_Laser_B			\Default Work Unit\Play_Diva_Laser_B	
	1722296163	Play_Massive_Laser_Low			\Default Work Unit\Play_Massive_Laser_Low	
	2724223031	Play_Massive_Laser_High			\Default Work Unit\Play_Massive_Laser_High	
	3187507146	Play_Test			\Default Work Unit\Play_Test	
	3429132251	Play_Wakaranai			\Default Work Unit\Play_Wakaranai	
	4042141136	Play_Drone			\Default Work Unit\Play_Drone	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	17043974	Test_LightSonification_01	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\ELECTRICITY-BUZZ_6EXT8-10793_A17F3635.wem		\Actor-Mixer Hierarchy\Default Work Unit\Test_LightSonification\Test_LightSonification_01		1329032
	22049188	Massive_Laser_High_02	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_High_02_1AD109C5.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_High\Massive_Laser_High_02		97984
	133284992	Massive_Laser_Low_05	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_Low_05_0D2E3DCB.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_Low\Massive_Laser_Low_05		62288
	189037822	DroneMotors2_Close	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors2_Close_DA443763.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorClose\DroneMotors2_Close		886552
	268957103	DroneMotors4_Constant	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors4_Constant_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorDonstant\DroneMotors4_Constant		572508
	286424811	Massive_Laser_High_04	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_High_04_2C213C29.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_High\Massive_Laser_High_04		116000
	291378622	Test_LightSonification_02	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\ELECTRICITY-BUZZ_6EXT8-10793_F55230E9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Test_LightSonification\Test_LightSonification_02		3322488
	301324913	Massive_Laser_High_03	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_High_03_5F2549EA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_High\Massive_Laser_High_03		84312
	320359399	Test_LightSonification	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\ELECTRICITY-BUZZ_6EXT8-10793_9519CFFD.wem		\Actor-Mixer Hierarchy\Default Work Unit\Test_LightSonification\Test_LightSonification		1555240
	348398810	Diva_Laser_A_03	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_A_03_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_A\Diva_Laser_A_03		166096
	363078031	DroneBuzz2	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneBuzz2_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneBuzz\DroneBuzz2		3018348
	369519667	Massive_Laser_Low_01	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_Low_01_CCF335A0.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_Low\Massive_Laser_Low_01		85096
	375036230	DroneMotors5_Constant	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors5_Constant_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorDonstant\DroneMotors5_Constant		798736
	403149729	Diva_Laser_B_05	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_B_05_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_B\Diva_Laser_B_05		149812
	417277163	DroneBuzz3	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneBuzz3_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneBuzz\DroneBuzz3		617360
	503829826	DroneMotors3_Close	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors3_Close_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorClose\DroneMotors3_Close		1020560
	693251547	Massive_Laser_Low_02	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_Low_02_0D660A09.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_Low\Massive_Laser_Low_02		83680
	697175471	DroneMotors1_Constant	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors1_Constant_8C45DBCA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorDonstant\DroneMotors1_Constant		692464
	762750778	Diva_Laser_A_05	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_A_05_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_A\Diva_Laser_A_05		159524
	784274264	DroneBuzz1	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneBuzz1_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneBuzz\DroneBuzz1		1562988
	786712485	Massive_Laser_Low_04	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_Low_04_A438A80E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_Low\Massive_Laser_Low_04		71888
	791316722	Diva_Laser_B_03	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_B_03_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_B\Diva_Laser_B_03		164200
	804369043	DroneMotors5_Close	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors5_Close_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorClose\DroneMotors5_Close		798736
	850963123	Massive_Laser_Low_03	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_Low_03_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_Low\Massive_Laser_Low_03		112152
	895531474	Diva_Laser_B_04	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_B_04_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_B\Diva_Laser_B_04		143196
	895948795	Massive_Laser_High_05	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_High_05_1906B146.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_High\Massive_Laser_High_05		104568
	907580411	DroneMotors3_Constant	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors3_Constant_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorDonstant\DroneMotors3_Constant		1020560
	935516517	DroneMotors2_Constant	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors2_Constant_D45F87C0.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorDonstant\DroneMotors2_Constant		886552
	957301454	Diva_Laser_A_01	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_A_01_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_A\Diva_Laser_A_01		98916
	965648641	Diva_Laser_B_01	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_B_01_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_B\Diva_Laser_B_01		136188
	977081786	Diva_Laser_A_02	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_A_02_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_A\Diva_Laser_A_02		151608
	1019883698	Massive_Laser_High_01	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Massive_Laser_High_01_4500B972.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Massive_Laser_High\Massive_Laser_High_01		82152
	1024702918	DroneMotors1_Close	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\DroneMotors1_Close_25EB5830.wem		\Actor-Mixer Hierarchy\Default Work Unit\Drone\Drone\DroneMotorClose\DroneMotors1_Close		692464
	1042232461	Diva_Laser_A_04	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_A_04_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_A\Diva_Laser_A_04		166684
	1060678933	Diva_Laser_B_02	C:\survivors-light\Assets\Wwise_Authoring\.cache\Windows\SFX\Diva_Laser_B_02_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Lasers\Diva_Laser_B\Diva_Laser_B_02		174528

