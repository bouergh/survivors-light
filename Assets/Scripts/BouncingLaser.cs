﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(LineRenderer))]
public class BouncingLaser : MonoBehaviour
{

    public int laserDistance;
    public int MaxReflection;
    public ChangeLight light;
    public LineRenderer mLineRenderer;
    public string bounceTag;
    public int maxBounce;
    private float timer = 0;

    // Use this for initialization
    void Start()
    {
        light = GameObject.FindGameObjectWithTag("Drone").GetComponent<ChangeLight>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!mLineRenderer.enabled)
        {
            timer = 0;
            StartCoroutine("FireMahLazer");
        }
        DrawLaser();
    }

    
    void DrawLaser()
    {
        

        int laserReflected = 1; //How many times it got reflected
        int vertexCounter = 1; //How many line segments are there
        bool loopActive = true; //Is the reflecting loop active?
        Vector2 laserDirection = new Vector2(1,0); //direction of the next laser
        Vector2 lastLaserPosition = transform.position; //origin of the next laser

        mLineRenderer.SetVertexCount(1);
        mLineRenderer.SetPosition(0, transform.position);

        

        while (loopActive)
        {

            int mask = LayerMask.GetMask("Ground", "Player", "Powers");
            RaycastHit2D hit = Physics2D.Raycast(lastLaserPosition, laserDirection, laserDistance, mask);
            //lastLaserPosition = hit.point + (laserDirection * 10);

            if (hit && ((hit.collider.gameObject.tag == "Power" && light.getRotate() == 2) || hit.collider.gameObject.tag == "Ground"))
            {
                
                
                laserReflected++;
                vertexCounter += 3;
                mLineRenderer.SetVertexCount(vertexCounter);
                mLineRenderer.SetPosition(vertexCounter - 3, Vector2.MoveTowards(hit.point, lastLaserPosition, 0.01f));
                mLineRenderer.SetPosition(vertexCounter - 2, hit.point);
                mLineRenderer.SetPosition(vertexCounter - 1, hit.point);
                lastLaserPosition = hit.point;
                laserDirection = Vector2.Reflect(laserDirection, hit.normal);
                
                hit = Physics2D.Raycast(lastLaserPosition, laserDirection, laserDistance, mask);

                
                Debug.DrawRay(lastLaserPosition, laserDirection, Color.blue, 5);
            }
            else
            {
                laserReflected++;
                vertexCounter++;
                mLineRenderer.SetVertexCount(vertexCounter);
                if(hit.collider.gameObject.tag != "Drone")
                    mLineRenderer.SetPosition(vertexCounter - 1, hit.point);
                else
                    mLineRenderer.SetPosition(vertexCounter - 1, lastLaserPosition + (laserDirection.normalized * laserDistance));


                loopActive = false;
            }
            if (laserReflected > laserDistance || laserReflected >= maxBounce )
                loopActive = false;

            
        }
    }
    
    
    IEnumerator FireMahLazer()
    {
        //Debug.Log("Running");
        mLineRenderer.enabled = true;
        int laserReflected = 1; //How many times it got reflected
        int vertexCounter = 1; //How many line segments are there
        bool loopActive = true; //Is the reflecting loop active?

        Vector2 laserDirection = new Vector2(1,-1); //direction of the next laser
        Vector2 lastLaserPosition = transform.localPosition; //origin of the next laser

        mLineRenderer.SetVertexCount(1);
        mLineRenderer.SetPosition(0, transform.position);
        RaycastHit2D hit = Physics2D.Raycast(lastLaserPosition, laserDirection);
        lastLaserPosition = hit.point - (laserDirection * 10);
        while (loopActive)
        {
            
            if (Physics2D.Raycast(lastLaserPosition, laserDirection, laserDistance) && hit.transform.gameObject.tag == bounceTag)
            {
                
                Debug.Log("Bounce");
                laserReflected++;
                vertexCounter += 3;
                mLineRenderer.SetVertexCount(vertexCounter);
                mLineRenderer.SetPosition(vertexCounter - 3, Vector3.MoveTowards(hit.point, lastLaserPosition, 0.01f));
                mLineRenderer.SetPosition(vertexCounter - 2, hit.point);
                mLineRenderer.SetPosition(vertexCounter - 1, hit.point);
                mLineRenderer.SetWidth(.1f, .1f);
                lastLaserPosition = hit.point;
                laserDirection = Vector2.Reflect(laserDirection, hit.normal);
            }
            else
            {

                Debug.Log("No Bounce");
                laserReflected++;
                vertexCounter++;
                mLineRenderer.SetVertexCount(vertexCounter);
                Vector2 lastPos = lastLaserPosition + (laserDirection.normalized * laserDistance);
                Debug.Log("InitialPos " + lastLaserPosition + " Last Pos" + lastPos);
                mLineRenderer.SetPosition(vertexCounter - 1, lastLaserPosition + (laserDirection.normalized * laserDistance));

                loopActive = false;
            }
            if (laserReflected > maxBounce)
                loopActive = false;
            
        }

        if (Input.GetKey("space") && timer < 2)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
            StartCoroutine("FireMahLazer");
        }
        else
        {
            yield return null;
            mLineRenderer.enabled = false;
        }
    }
    
    
}

