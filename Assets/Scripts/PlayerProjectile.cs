﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : GenericProjectile {
	
	private Light lum;
	private Color droneColor;
	[SerializeField]
	private float timer;
	[SerializeField]
	private float Maxtime;

	private Drone drone;

	// Use this for initialization
	void OnEnable () {
		lum = GetComponentInChildren<Light>();
		GameObject gDrone = GameObject.FindGameObjectWithTag("Drone");
		if (gDrone)
		{
			drone = gDrone.GetComponent<Drone>();
			droneColor = drone.GetLight().color;
			lum.color = droneColor;
			GetComponent<SpriteRenderer>().color = droneColor;
		}
			
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		gameObject.tag = "Projectile";
	}

	private void Update()
	{
	   if (timer < Maxtime && !gameObject.GetComponent<SpriteRenderer>().enabled)
		{
			timer += Time.deltaTime;
		}
		else
		{
			gameObject.GetComponent<SpriteRenderer>().enabled = true;
		}

	}

}
