﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LightMenue : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField]
    private SpecialLight specialLight;

    [SerializeField]
    private int indexLight, i;

    [SerializeField]
    private ChangeLight _changeLight;

    [SerializeField]
    private Color normalColor, deactivatedColor;

    [SerializeField] private Image image;

    [SerializeField]
    private Text PanelDescription;

    [SerializeField]
    private string description;
    private string initialText;

    private Button btn;

	// Use this for initialization
	void Start () {
        normalColor = specialLight.activeColor;
        image = this.GetComponent<Image>();
        initialText = PanelDescription.text;
	    btn = this.GetComponent<Button>();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (specialLight.obtained)
        {
            initialText = PanelDescription.text;
            PanelDescription.text = description;
        }
        else
        {
            initialText = PanelDescription.text;
            PanelDescription.text = "Light not obtained";
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PanelDescription.text = initialText;
    }

   public void changeLight()
    {
        _changeLight.changeLight(i);
       
    }

    // Update is called once per frame
    void Update ()
    {
        i = indexLight + _changeLight.getRotate();
        switch (i)
        {
                case 5:
                    i = 0;
                    break;
                case 6:
                    i = 1;
                    break;
                case 7:
                    i = 2;
                    break;
                case 8:
                    i = 3;
                    break;
                default:
                    break;
        }

        specialLight = _changeLight.getLightArray(i);
        normalColor = specialLight.activeColor;
        description = specialLight.description;
        
        if (specialLight.obtained)
        {
            btn.interactable = true;
            image.color = normalColor;
        }
        else
        {
            image.color = deactivatedColor;
            btn.interactable = false;
        }
    }
}
