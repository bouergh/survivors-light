﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damage : MonoBehaviour {

	[SerializeField]
	private int maxLife = 100;
    [SerializeField]
    private float timer, invDelay = 2f, blinkTimer, blinkDelay = 0.1f;
	public int actualLife;
    private SpriteRenderer sprite;
    private Color changeColor;

    [SerializeField]
    private Slider slider;
    [SerializeField]
    private Text text;

    private EnemyScrapDrop enemyScrap;
	//public int push;
	private bool alive;
    private bool invincible = false;

	// Use this for initialization
	void Start () {
		//init life, maybe refer to something else is player is to be respawned with remaining life at some point ?
		actualLife = maxLife;
		alive = true;
		enemyScrap = this.GetComponent<EnemyScrapDrop>();
        sprite = this.GetComponent<SpriteRenderer>();
        changeColor = sprite.color;
        UpdateView();
	}
    

    public void UpdateView()
    {
        if(tag == "Enemy" && slider)
        {
            slider.value = 100* actualLife / maxLife; //100 is arbitrary value from editor don't know how to get
        }
        else if(tag == "Player" && text)
        {
            text.text = "HP = " + actualLife;
        }
    }

	public void TakeDamage(int damage/*, int x*/)   //need to make player blink when taking damage, or something to let the player know he suffers
	{
		
		if (alive) //don't decrease life if already dead, so don't proc death multiple times
		{
			actualLife -= damage;
			Debug.Log("Perte de " + damage + " points de vie pour "+gameObject.name+" !");
			//Vector2 pushForce = new Vector2 (push, push);
			//this.GetComponent<Rigidbody2D> ().AddForce (pushForce, ForceMode2D.Impulse);
			if (actualLife <= 0)
			{
				alive = false;
				Death();
			}
		}
        UpdateView();


    }

    public void TakeDamage(int damage, int x, float push)   //need to make player blink when taking damage, or something to let the player know he suffers
    {

        if (alive && !invincible) //don't decrease life if already dead, so don't proc death multiple times
        {
            actualLife -= damage;
            //Debug.Log("Perte de " + damage + " points de vie pour " + gameObject.name + " !");
            Vector2 pushForce = new Vector2(push*x, push);
            this.GetComponent<Rigidbody2D>().AddForce(pushForce, ForceMode2D.Impulse);
            invincible = true;
            if (actualLife <= 0)
            {
                alive = false;
                Death();
            }
        }
        UpdateView();
    }

    public void Death()
	{
		//procs death
		//Debug.Log("Le joueur vient de mourir :'( Press F to pay Respects");
		if (this.tag == "Enemy")
		{
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			gameObject.GetComponent<EnemyScrapDrop>().dropScraps();
			//gameObject.SetActive(false);
		}
	}

	public bool Heal(int life)
	{
		if(alive && actualLife < maxLife) //vivant et pas full hp
		{
			actualLife += life;
            UpdateView();
            return true; //if has been healed, so the health pack disappears, for ex.
            
        }
        else
		{
			return false;
		}
        
    }

	public IEnumerator Repousse(Vector2 pushForce, float pushTime)
	{
		GetComponent<Mouvement>().beingPushed = true;
		this.GetComponent<Rigidbody2D> ().AddForce (pushForce, ForceMode2D.Impulse);
		yield return new WaitForSeconds(pushTime); //formula to link this time and the force, instead of passing argument ?
		GetComponent<Mouvement>().beingPushed = false;
	}

	// Update is called once per frame
	void Update () {
        if (this.tag == "Enemy")
        {
            if (enemyScrap && enemyScrap.allDropped)
                gameObject.SetActive(false);
        }
        if (invincible)
        {
            timer += Time.deltaTime;
            blinkTimer += Time.deltaTime;
            if(blinkTimer>= blinkDelay)
            {
                if (sprite.enabled)
                {
                    sprite.enabled = false;
                    blinkTimer = 0;
                }
                else
                {
                    sprite.enabled = true;
                    blinkTimer = 0;
                }
                    
            }
            if (timer >= invDelay)
            {
                blinkTimer = 0;
                timer = 0;
                invincible = false;
                sprite.enabled = true;
            }
        }
        
	}
}
