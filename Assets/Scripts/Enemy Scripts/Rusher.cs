﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rusher : MonoBehaviour {

	private Transform target;
	public float range;
	public float pushForce;
	private int x;
	public float speed;
	public int damage;

	// Use this for initialization
	void Start () {

		target = GameObject.FindGameObjectWithTag ("Player").transform;
		
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			Damage damageScript = other.GetComponent<Damage>();
			if (damageScript != null) damageScript.TakeDamage(damage/*, x*/);
			//Vector2 push = new Vector2 (pushForce, )*x;
			//other.GetComponent<Rigidbody2D> ().AddForce (push, ForceMode2D.Impulse);
			Destroy (this.gameObject);
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float distance = this.transform.position.x - target.position.x;
		//Debug.Log ("Le hero est a " + distance + "Du rusher");
		if (Mathf.Abs (distance)<=range) {
			if (distance < 0) {
				x = 1;
			}else{
				x=-1;
			}
			Vector3 mvt = new Vector3 (x, 0, 0)*speed;
			this.gameObject.transform.Translate (mvt);
		}
		
	}
}
