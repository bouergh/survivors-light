﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

    private Animator anim;
    private Transform player;
    private Transform projectileSpawn;
    public float cooldown = 1f;
    private float timer;
    public float vitesseDeTir = 50f;
    public GameObject prefabProjectile;
    
    public string projectileTag;

    private enum State
    {
        Idle,
        Range,
        Attack,
    }
    private State state;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        player = GameObject.Find("Hero").transform;
        state = State.Idle;
        projectileSpawn = transform.GetChild(0);
        timer = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        
        if(state == State.Range)
        {
            FindPlayer();
        }
        if (state == State.Attack)
        {
            GunRotation();
            if(timer > cooldown)
            {
                timer = 0f;
                Attack();
            }
            else
            {
                timer += Time.deltaTime;
            }
        }
    }
    private void FindPlayer()
    {
        Debug.Log("turrets seeks player");
        //raycast to go in attack state
        RaycastHit2D ray = Physics2D.Raycast(projectileSpawn.position, player.position-transform.position,Mathf.Infinity);
        if(ray)
        {
            Debug.Log(ray.collider.gameObject.name);
            state = State.Attack;
        }
    }
    private void Attack()
    {
        //launch a projectile in player direction
        Debug.Log("turret attacks !");
        Vector2 directionDeTir = player.position - projectileSpawn.position;
        float angle = (Mathf.Atan(directionDeTir.y / directionDeTir.x)) * Mathf.Rad2Deg;
        GameObject projectile = ObjectPooler.SharedInstance.GetPooledObject(projectileTag);
        if (projectile != null)
        {
            projectile.transform.position = projectileSpawn.position;
            projectile.transform.rotation = transform.rotation;
            projectile.SetActive(true);
        }
        projectile.GetComponent<Rigidbody2D>().velocity = directionDeTir.normalized * vitesseDeTir;
    }
    private void GunRotation()///// Mouvement :
    {
        Vector2 localCrosshair = transform.InverseTransformPoint(player.position);
        //Debug.Log(localCrosshair.y);
        if (localCrosshair.x < -localCrosshair.y / 2f)
        {
            anim.Play(0, -1, 0.1f);
        }
        else if (localCrosshair.x < localCrosshair.y / 2f)
        {
            anim.Play(0, -1, 0.5f);
        }
        else
        {
            anim.Play(0, -1, 0.9f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("trigger coll between " +name+ " and " + collision.name);
        if(collision.tag == "Player")
        {
            state = State.Range;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            state = State.Idle;
        }
    }
}
