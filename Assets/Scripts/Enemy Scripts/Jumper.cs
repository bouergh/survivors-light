﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : MonoBehaviour {

    public float jumpForce, jumpX;
    [SerializeField]
    private float speedX, distX, maxX, minX, pushForce;
    [SerializeField]
    private int facing, x, damage;
    private bool idle;
    private Transform groundCheck;
    private Collider2D rc;

    private enum State
    {
        GOINGLEFT,  //Va ver la droite
        GOINGRIGHT,
        ATTACK,
    }

    private State state = State.GOINGLEFT;

    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable()
    {
        groundCheck = this.transform.GetChild(0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject target = collision.gameObject;
        if(target.tag == "Player" && rc)
        {
            if (target.transform.position.x < this.transform.position.x)
                facing = 1;
            else
                facing = -1;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(jumpX*facing, jumpForce), ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        Collider2D other = coll.collider;
        if (other.tag == "Player")
        {
            if (other.gameObject.transform.position.x < this.transform.position.x)
            {
                x = -1;
            }
            else
            {
                x = 1;
            }

            Damage damageScript = other.GetComponent<Damage>();
            if (damageScript != null) damageScript.TakeDamage(damage, x, pushForce);
        }
    }

    // Update is called once per frame
    void Update () {
        Collider2D coll = groundCheck.GetComponent<Collider2D>();
        rc = Physics2D.OverlapCircle(groundCheck.transform.position, coll.bounds.extents.x, 1 << LayerMask.NameToLayer("Ground"));
        if (state == State.GOINGLEFT)
        {
            transform.Translate(-speedX, 0, 0);
            distX -= speedX;
        }else if(state == State.GOINGRIGHT)
        {
            transform.Translate(speedX, 0, 0);
            distX += speedX;
        }
        if (distX >= maxX)
        {
            state = State.GOINGLEFT;
        }
        else if(distX <= minX)
        {
            state = State.GOINGRIGHT;
        }
	}
}
