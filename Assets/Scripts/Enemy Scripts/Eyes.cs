﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyes : MonoBehaviour {

    private float dist, diffX, theta, diffY;

    public Transform target;
    private float xFix = 0;
    private Vector3 angle;

	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("Player").transform;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject coll = collision.gameObject;
        if(coll.tag == "BossPlatform")
        {
            coll.GetComponentInParent<PlatFormBoss>().Active();
        }
        if(coll.tag == "BossTurret")
        {
            coll.GetComponent<BossTurret>().shoot = true;
        }
        if(coll.tag == "Player")    //voir joueur pour tirer, meme range que allumer plateformes pour les bouger ??
        {
            GetComponentInParent<Face>().seePlayer++;
        }
    }

    //il faut aussi desactiver quand on s'eloigne sinon les trucs ne se font qu'une fois et si le joueur reste immobile rien ne se passe plus !
    //en tout cas pour les plateformes ca parait nul
    //a voir pour les tourelles si c'est pas trop cheate
    private void OnTriggerExit2D(Collider2D collision)
    {
        GameObject coll = collision.gameObject;
        if (coll.tag == "BossPlatform")
        {
            coll.GetComponentInParent<PlatFormBoss>().Desactive();
        }
        if (coll.tag == "BossTurret")
        {
            coll.GetComponentInParent<BossTurret>().shoot = false;
        }
        if (coll.tag == "Player")
        {
            GetComponentInParent<Face>().seePlayer--;
        }
    }

    // Update is called once per frame
    void Update () {
        //bout de code pour tourner le regard vers le joueur !
        dist = Vector3.Distance(transform.position, target.position);
      
        diffX = target.position.x - transform.position.x ;
        diffY = transform.position.y - target.position.y;
        if(diffX < 0)
        {
            xFix = -1;
        }
        else
        {
            xFix = 1;
        }
        
        theta = xFix*(Mathf.Acos( diffY / dist) ) * Mathf.Rad2Deg;
        //Debug.Log(theta);
        angle = new Vector3(0, 0, theta);
        transform.eulerAngles = angle;
	}
}
