﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crawler : MonoBehaviour {


	private enum State
	{
		Ceiling,   //same as ceiling + can fall
		Passive,    
		Aggressive,  //can only be aggressive if on the floor already
	}
	private enum MoveState
	{
		GOINGLEFT,  //Va vers la droite
		GOINGRIGHT,
		STADSTILL,
	}

	[SerializeField]
	private State state = State.Ceiling;
	private MoveState mState = MoveState.GOINGLEFT;
	[SerializeField]
	private float speedX, distX, maxX, minX;
    Vector3 originalScale;
    Vector3 theScale;
    private Animator anim;
	private Transform player;
	public float pushForce;
	private int x;
	public float speed;
	public int damage;
	public int xVelocity = 1;
	[SerializeField]
	private bool floor, stoped;

	private Transform groundCheck;

	private Collider2D rc;

	[SerializeField] private GameObject target;
	//public bool goRightAtStart = false;//so he starts going on his right

	//private ContactPoint2D[] contactPoints = new ContactPoint2D[50]; //already allocated array to store contact points for ground collision
	//public float xMargin = 0.2f; //margin from where we detect a contactpoint to be on left or right of object and not just underneath or on top (to detect walls !)

	// Use this for initialization
	void OnEnable () {
		//init player object
		player = GameObject.FindGameObjectWithTag ("Player").transform;
        anim = this.GetComponent<Animator>();
		groundCheck = this.transform.GetChild(0);
		//start by default on ceiling
		if (floor)
		{
			//GetComponent<Rigidbody2D>().gravityScale = 3;
			state = State.Passive;
			Fall();
			this.GetComponents<PolygonCollider2D>()[0].enabled = false;
			//this.transform.localScale = new Vector3(1,1,-1);
		}
		else
		{
			GetComponent<Rigidbody2D>().gravityScale = -1;
			this.GetComponents<PolygonCollider2D>()[1].enabled = false;
			state = State.Ceiling;
		}

		if (stoped)
			mState = MoveState.STADSTILL;

		originalScale = this.transform.localScale;
		
	}


	
	// Update is called once per frame
	void FixedUpdate () {

		Collider2D coll = groundCheck.GetComponent<Collider2D>();
		rc = Physics2D.OverlapCircle(groundCheck.transform.position, coll.bounds.extents.x, 1 << LayerMask.NameToLayer("Ground"));
		Move();
	}

	private void Fall()
	{
		GetComponent<Rigidbody2D>().gravityScale = 1;
        theScale = transform.localScale;
        theScale.y *= -1;
        transform.localScale = theScale;
		floor = true;
		
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		//pour le collider de tombage sur le joueur en cone
		if(collision.tag == "Player" && state == State.Ceiling)
		{
			Fall();
            this.GetComponents<PolygonCollider2D>()[0].enabled = false;
			this.GetComponents<PolygonCollider2D>()[1].enabled = true;
			target = collision.gameObject;
			
		}else if (collision.tag == "Player" && state == State.Passive)
		{
			target = collision.gameObject;
			state = State.Aggressive;
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		//target = collision.gameObject;
		if(other.tag == "Player")
			state = State.Passive;
	}

	void Move()
	{
		if (state == State.Ceiling || state == State.Passive)
		{
			if (mState == MoveState.GOINGLEFT)
			{
				transform.Translate(-speedX, 0, 0);
				distX -= speedX;
			}
			else if (mState == MoveState.GOINGRIGHT)
			{
				transform.Translate(speedX, 0, 0);
				distX += speedX;
			}

			if (distX >= maxX && !stoped)
			{
				theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				mState = MoveState.GOINGLEFT;
			}
			else if (distX <= minX && !stoped)
			{
				theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
				mState = MoveState.GOINGRIGHT;
			}
		}
		else if(rc)
		{
			Vector3 pPosition = target.transform.position;
			pPosition.y = this.transform.position.y;
			//pPosition.z = 0;
			
			Vector3 step = Vector3.MoveTowards(transform.position, pPosition, speed);
			
			this.GetComponent<Rigidbody2D>().MovePosition(step);
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		Collider2D other = coll.collider;
        if (other.tag == "Player")
        {
            if (other.gameObject.transform.position.x < this.transform.position.x)
            {
                x = -1;
            }
            else
            {
                x = 1;
            }
            Damage damageScript = other.GetComponent<Damage>();
            if (damageScript != null) damageScript.TakeDamage(damage, x, pushForce);
        }

        /* un peu compliqué tout ça, on va prendre l'approche de Michael sur le Jumper.cs, aka bouger d'une distance fixe
		if(other.tag == "Ground")   //comment eviter que cela compte les collisions avec le sol ?
		{
			//on va verifier le point de collision pour savoir si on touche simplement le sol/plafond, ou si un des murs, lequel :

			coll.GetContacts(contactPoints);
			foreach(ContactPoint2D contact in contactPoints)
			{
				if(contact.point.x > transform.position.x + xMargin)    //contact point is on the right, let's go left
				{
					GetComponent<Rigidbody2D>().velocity = new Vector2(-xVelocity, 0f);
				}
				else if (contact.point.x < transform.position.x - xMargin)    //contact point is on the left, let's go right
				{
					GetComponent<Rigidbody2D>().velocity = new Vector2(xVelocity, 0f);
				}
			}

			
		}
		*/


       /* if (other.tag == "Player")
		{
			Damage damageScript = other.GetComponent<Damage>();
			if (damageScript != null) damageScript.TakeDamage(damage);
			//Vector2 push = new Vector2 (pushForce, )*x;
			//other.GetComponent<Rigidbody2D> ().AddForce (push, ForceMode2D.Impulse);
			//Destroy(this.gameObject);
		}*/
	}
}
