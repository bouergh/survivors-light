﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogTrigger : MonoBehaviour {

    
    public string[] texts;
    private TextScript textScript;
    public bool playerClose = false; //is the player inside the dialog zone ? could be true on some object so we set in editor (player spawns inside trigger)
    protected bool hasTalked = false;

    //label on top of pnj
    /* now useless : manage the canvas in Editor
    public Vector2 labelDeltaPosition = new Vector2(-1f, 1f);
    public string labelString = "Press ENTER";
    public GUIStyle style;
    */

	// Use this for initialization
	void Start () {
        textScript = GameObject.Find("DialogBox").GetComponentInChildren<TextScript>(); ;//must be only one in a scene ! (one text on screen at a time anyway)

        //GUI Style defaults :
        /*
        style.fontSize = 30;
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.black;
        */
    }
	

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.tag == "Player" )
        {
            //Debug.Log("player in zone");
            playerClose = true;
            hasTalked = false; //reset this ! a l'air OK

            /*
            if (!hasTalked)
            {
                StartCoroutine(Dialog(0));
            }
            else
            {
                StartCoroutine(RepeatDialog());
            }
            */
            //en fait on va faire des zones petites et obliger le joueur a taper la touche pour voir le dialogue c'est plus simple pour lui et moi
            StartCoroutine(RepeatDialog());

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
        if (collision.tag == "Player")
        {
            //Debug.Log("player out zone");
            playerClose = false;
        }

    }


    //code to call a coroutine to wait for key down with :      yield return StartCoroutine(WaitForKeyOrPlayerOut(KeyCode.Return))
    protected IEnumerator WaitForKeyOrPlayerOut(KeyCode keyCode)
    {
        while (!Input.GetKeyDown(keyCode) && playerClose)
            yield return null;
    }


    protected IEnumerator Dialog(int number)  //since there will be only one text box, we will interact with the script on it and wait for others texts to be discarded
    {
        string text = texts[number];

        yield return StartCoroutine(Display(text)); //coroutine to wait for unlock of dialog box and then display

        yield return new WaitForEndOfFrame();   //wait so we don't skip all text at once
        yield return StartCoroutine(WaitForKeyOrPlayerOut(KeyCode.Return)); //pressing button to next text

        textScript.Unlock();

        
        //if player still close, and there is next message, show it. Else finish the coroutine
        if (number < texts.Length-1)
        {
            //Debug.Log("number is " + number + " and length is " + (texts.Length));
            if (playerClose) StartCoroutine(Dialog(number + 1));
        }
        else
        {
            hasTalked = true;
            //Debug.Log("number is "+number+" and length is "+ (texts.Length));
            //Debug.Log("dialog finished for good");
            StartCoroutine(RepeatDialog());
        }
        
    }

    protected IEnumerator RepeatDialog()  //possibility to play dialog again by pressing enter when close to character (or always like that if no auto)
    {
        //Debug.Log("press enter to play dialog again !");
        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(WaitForKeyOrPlayerOut(KeyCode.Return));
        //Debug.Log("starting dialog again !");
        if (playerClose) StartCoroutine(Dialog(0));
        //Debug.Log("starting dialog again !");
    }

    private IEnumerator Display(string text)
    {
        while (!textScript.Display(text))
            yield return null;
    }

    /* useless, better to add a canvas so we can sort them all !
    void OnGUI()    //display some "press enter" above PNJ, so player can display text
    {
        GUI.depth = 10; //so that i won't be in front of the dialog box
        Vector3 getPixelPos = Camera.main.WorldToScreenPoint((Vector2)transform.position + labelDeltaPosition);
        getPixelPos.y = Screen.height - getPixelPos.y;
        GUI.Label(new Rect(getPixelPos.x, getPixelPos.y, 200f, 100f), labelString, style);
    }
    */


}
