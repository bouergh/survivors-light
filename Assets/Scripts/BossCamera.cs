﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCamera : MonoBehaviour {

    public bool fight = false;
    public int bossSize, normalSize;
    public PerfectPixel camera;
	// Use this for initialization
	void Start () {
		camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PerfectPixel>();

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !fight)
        {
            fight = true;
        }else if (other.CompareTag("Player"))
        {
            fight = false;
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
