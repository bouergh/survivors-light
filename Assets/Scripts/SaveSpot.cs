﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveSpot : DialogTrigger
{
    public int savePointIndex; //to find this save point afterwards
    public int saveMenuIndex; //to load the right LoadScene/SaveScene
    public GameObject hero; //need hero to Game.current

    void Awake()
    {
        hero = GameObject.Find("Hero");
    }

    // Update is called once per frame
    void Update()
    {    //add this to give the drone with other script
        if (hasTalked)
        {
            Debug.Log("saving game");
            SaveGame();
            //hasTalked = false;
        }

    }

    private void SaveGame()
    {
        //create a current game if not existant
        if(Game.current == null)
        {
            Game.current = new Game();
        }
        //update the Game.current singleton with actual data
        //no name : defined by save time
        Game.currentState.sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Game.currentState.savePointIndex = savePointIndex;
        Game.currentState.actualHealth = hero.GetComponent<Damage>().actualLife;
        //Game.current.item1Number;
        //Game.current.item2Number;
        //Game.current.item3Number;
        //load the LoadScene to manage saves and files
        SceneManager.LoadScene(saveMenuIndex);
    }

}
