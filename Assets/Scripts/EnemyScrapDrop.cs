﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScrapDrop : MonoBehaviour
{
	[SerializeField]
	private int ammountOfScraps, nDropped;
	private bool drop = false;
    private int x;
	public bool allDropped = false;
	[SerializeField]
	private float delay, timer;

	// Use this for initialization
	void Start () {
		
	}

    

    public void dropScraps()
	{
		drop = true;
		for (int i = 0; i < ammountOfScraps; i++)
		{
			
		}
	}
	// Update is called once per frame
	void Update ()
	{
		if(drop)
			timer += Time.deltaTime;
		if (nDropped <= ammountOfScraps && timer >= delay)
		{
			GameObject scrap = ObjectPooler.SharedInstance.GetPooledObject("Scrap");
			scrap.transform.position = this.transform.position;
			scrap.SetActive(true);
			nDropped++;
			timer = 0;
		}

		if (nDropped == ammountOfScraps)
		{
			allDropped = true;
            this.gameObject.SetActive(false);
		}
	}
}
