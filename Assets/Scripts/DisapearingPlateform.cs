﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisapearingPlateform : TriggerObject {


    [SerializeField]
    private GameObject platform;
    public int childIndex;

    public override void Active()
    {
        active = true;
        platform.SetActive(true);
    }

    public override void Desactive()
    {
        active = false;
        platform.SetActive(false);
    }

    // Use this for initialization
    void Start () {
        platform = transform.GetChild(childIndex).gameObject;
        platform.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
