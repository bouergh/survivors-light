﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTurret : MonoBehaviour {

    public GameObject PrefabProjectile;
    private float range, speed, dist;
    public Transform target;
    private Transform projectileSpawn;
    public float VitesseDeTir = 75f, timer, MaxTime;
    public string projectileTag;
    public bool shoot;

    // Use this for initialization
    void Start() {
        projectileSpawn = transform.GetChild(0);
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    

    // Update is called once per frame
    void Update() {
        if (shoot)
        {
            timer += Time.deltaTime;
        }
        if (timer >= MaxTime)
        {
            Debug.Log("Shoot");
            Vector2 directionDeTir = new Vector2(1, 0);
            //float angle = (Mathf.Atan(directionDeTir.y / directionDeTir.x)) * Mathf.Rad2Deg;
            GameObject projectile = ObjectPooler.SharedInstance.GetPooledObject(projectileTag);
            if (projectile != null)
            {
                projectile.transform.position = projectileSpawn.position;
                projectile.transform.rotation = transform.rotation;
                projectile.SetActive(true);
            }
            projectile.GetComponent<Rigidbody2D>().velocity = directionDeTir.normalized * VitesseDeTir;
            shoot = false;
            timer = 0;
        }
    }
}
