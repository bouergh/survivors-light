﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenueController : MonoBehaviour
{
	[SerializeField]
	private GameObject StatusMenue, OptionsMenue, controllMenue;

	// Use this for initialization
	void Start () {
		
	}

	public void enableMap()
	{
		if (OptionsMenue.active)
		{
			OptionsMenue.SetActive(false);
		}
		else
		{
			OptionsMenue.SetActive(true);
			StatusMenue.SetActive(false);
			
		}
	}

	public void enableStatus()
	{
		if (StatusMenue.active)
		{
			StatusMenue.SetActive(false);
		}
		else
		{
			StatusMenue.SetActive(true);
			OptionsMenue.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
