﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//will be called by multiple DialogTrigger.cs, sometimes while displaying one or another
public class TextScript : MonoBehaviour {


    private bool locked = false; //works as a mutex, so that new dialogs don't go over actual dialogs


    public bool Display(string text)
    {
        if (!locked)
        {   //when displaying text, shows the box in parent, and the text
            GetComponentInParent<Image>().enabled = true;
            GetComponent<Text>().enabled = true;
            GetComponent<Text>().text = text;
            locked = true;
        }
        return locked;
    }

    public void Unlock()
    {
        //Debug.Log("unlocking dialog box");
        //hide the box and unlock its display for new dialog
        GetComponent<Text>().text = "";
        GetComponent<Text>().enabled = false;
        GetComponentInParent<Image>().enabled = false;
        locked = false;
    }

    
	// Update is called once per frame
	void Update () {
	}

}
