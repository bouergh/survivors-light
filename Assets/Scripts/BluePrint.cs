﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BluePrint : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

	public int Cost;
    public bool obtained = false;
    public Button button;
	public Text PanelDescription;
    public GameObject tempBPTest;
    public Inventaire inventaire;
    private string initialText;
	public string description;

	// Use this for initialization
	void Start () {
        inventaire = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventaire>();
        //button = this.GetComponent<Button>();
        button.interactable = false;
        PanelDescription.text = initialText;
    }

    public void activateBTN()
    {
        button.interactable = true;
        obtained = true;
    }

	public void OnPointerEnter(PointerEventData eventData)
	{
        if (obtained)
        {
            initialText = PanelDescription.text;
            PanelDescription.text = description + ", Costs : " + Cost + " Scraps";
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PanelDescription.text = initialText;
    }

	// Update is called once per frame
	void Update () {
		
	}
}
