﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
	private Damage d;
	private int life;
	private Text text;
	// Use this for initialization
	void Start ()
	{
		d = GameObject.FindGameObjectWithTag("Player").GetComponent<Damage>();
		text = GetComponent<Text>();

	}
	
	// Update is called once per frame
	void Update ()
	{
		life = d.actualLife;
		text.text = "HP : " + life;
	}
}
