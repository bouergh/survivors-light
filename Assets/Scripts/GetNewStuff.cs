﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetNewStuff : DialogTrigger {


    [SerializeField]
    GameObject trapLights;

    [SerializeField]
    GameObject[] newStuff;

    private bool stuffGiven = false;

    [SerializeField]
    private textSpawner _textSpawner;

    //[SerializeField]
    //private Light _light;

    private enum Stuff
    {
        Drone,
        Gun
    }
    [SerializeField]
    private Stuff whatStuff;
	
    void giveStuff()
    {
        Debug.Log("giving stuff");
        if (trapLights)
        {
            //TURN OFF THE LIGHTS
            /*
            foreach (Transform child in trapLights.transform)
            {
                child.GetComponentInChildren<Light>().enabled = false;
            }
            */
            /*foreach (Light child in trapLights.GetComponentsInChildren<Light>())
            {
                child.enabled = false;
            }*/
            trapLights.SetActive(false);
            
        }
        //will also work for the gun, actually
        foreach(GameObject gObj in newStuff)
        {
            gObj.SetActive(true);
        }

        hasTalked = false;
        stuffGiven = true;

        //update the gamestate for savefiles
      /*  switch (whatStuff)
        {
            case Stuff.Drone:
                Game.currentState.gotDrone = true;
                
                break;
            case Stuff.Gun:
                Game.currentState.gotGun = true;
                break;
            default:
                break;
        }*/

        this.GetComponentInChildren<Light>().enabled = false;
    }
    

    // Update is called once per frame
    void Update () {    //add this to give the drone with other script
        if (hasTalked && !stuffGiven)
        {
            giveStuff();
            _textSpawner.activateTutorial();
            
        }

	}

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            //Debug.Log("player out zone");
            playerClose = false;
            hasTalked = true;   //add this to make sure he gets the fucking item even if he goes back
        }

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            //Debug.Log("player in zone");
            playerClose = true;


            if (!hasTalked)
            {
                StartCoroutine(Dialog(0));
            }
            else
            {
                StartCoroutine(RepeatDialog());
            }

        }
    }
}
