﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Face : MonoBehaviour {

    //maxRand (entre 0 et 1) determine la valeur max de deplacement random ajoute au deplacement par rapport au joueur
    [SerializeField]
    private float rangeToPlayer = 15f, rangeToWalls = 3f, dist = 0f, speed =0.1f, diffX, diffY,
        timer, minTime = 1f, maxTime = 2f, chanceOfFire = 50f, VitesseDeTir = 50f, delais =1f;
    [SerializeField]
    private int missileShot = 0, nMissile = 3;
    public bool shooting = false;
    public int index;
    public string projectileName;
    [SerializeField]
    private Transform target;
    private Transform projectileSpawn;
    private const float EPSILON = 0.09999f;

    [SerializeField]
    private float maxRandDist = 0.2f, maxRandDelay = 1f, randTimer, randCount;
    private bool randomMoving = false;
    private Vector2 deplaceRand;

    public int seePlayer = 0; //two eyes with two Eyes.cs script so we need to make sure AT LEAST ONE sees player

	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        projectileSpawn = transform.GetChild(index);
        
        //set timer
        randTimer = Random.Range(0f, maxRandDelay);
        seePlayer = 0; //change if supposed to see player at start of fight ! Or will be buggy :/
    }

    void Shoot()
    {
        Vector2 directionDeTir = new Vector2(target.position.x - projectileSpawn.position.x, target.position.y - projectileSpawn.position.y).normalized;
        //float angle = (Mathf.Atan(directionDeTir.y / directionDeTir.x)) * Mathf.Rad2Deg;
        GameObject projectile = ObjectPooler.SharedInstance.GetPooledObjectByName(projectileName);
        if (projectile != null)
        {
            projectile.transform.position = projectileSpawn.position;
            projectile.transform.rotation = transform.rotation;
            projectile.SetActive(true);
        }
        projectile.GetComponent<Rigidbody2D>().velocity = directionDeTir.normalized * VitesseDeTir;
        timer = 0;
        missileShot++;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        ShootTime();

        Vector2 move = Move() + RandomMove();
        /*
        if (CanMove(move, rangeToWalls))
        {
            Translate(move); //won't translate if too close to a ground stuff
        }
        else
        {
            Debug.Log("too close to wall !");
        }
        */
        Translate(move);

    }

    private void Translate(Vector2 move)
    {
        //

        //raycast to find wall on the path
        //RaycastHit rc = Physics2D.Raycast(transform.position,move,)
        if (true)
        {
            transform.position += new Vector3(move.x, move.y, 0f);//apply the calculated transforms
        }
    }

    //doit tirer seulement quand il te voit sinon c'est abuse
    private void ShootTime()
    {
        timer += Time.deltaTime;

        //si deja attendu, on peut tirer chanceOfFire/100
        if (timer >= minTime && timer < maxTime && !shooting)
        {
            float attack = Random.Range(0f, 100f);
            if (attack <= chanceOfFire && seePlayer > 0)
            {
                shooting = true;
                timer = 0;
            }
        }
        //si temps max atteint, sur de tirer ET SI JOUEUR PROCHE CALISS
        else if (timer >= maxTime && !shooting && seePlayer>0)
        {
            shooting = true;
            timer = 0;
        }
        //si 'shooting' : on tire une balle tous les 'delais', jusqu'a en avoir tire 'nMissile'
        if (shooting && missileShot < nMissile)
        {
            if (timer >= delais)
            {
                Shoot();
            }
        }
        else if (missileShot >= nMissile && shooting)
        {
            shooting = false;
            missileShot = 0;
        }
    }

    private Vector2 Move()//need to add some random moves to make it realistic
    {
        dist = Mathf.Abs(Vector2.Distance(this.transform.position, target.position));
        diffX = target.position.x - transform.position.x;
        diffY = target.position.y - transform.position.y;
        
        Vector2 deplace = new Vector2(diffX, diffY).normalized * speed;
        if (dist > rangeToPlayer + EPSILON)
        {
            //speed = speed * Time.deltaTime;
            //old this.transform.Translate();
            return deplace;
        }
        else if (dist < rangeToPlayer - EPSILON)
        {
            //old this.transform.Translate(deplace * -1);
            return deplace * -1;
        }
        else
        {
            return Vector2.zero;
        }
    }

    private Vector2 RandomMove()
    {
        randCount += Time.deltaTime; //if in fixed update !

        //phase 1 : wait for delay and then define the distance
        if (!randomMoving)  //if timer is finished 
        {

            if (randCount > randTimer)
            {
                //gestion du deplacement aleatoire ajoute
                deplaceRand = new Vector2(Random.Range(-maxRandDist, maxRandDist), Random.Range(-maxRandDist, maxRandDist));
                
                randomMoving = true;

                //at this point the rand timer can be used for timing the movement
                randTimer = deplaceRand.magnitude / speed;
                randCount = 0f;
            }
            return Vector2.zero;
        }
        //phase 2 : move the distance and then define the delay
        else //moves frame by frame if initiated a random move before
        {
            //old this.transform.Translate(deplaceRand / randTimer);
            if (randCount > randTimer)
            {
                //reset timer and bool
                randTimer = Random.Range(0f, maxRandDelay);
                randCount = 0f;
                randomMoving = false;
            }
            return deplaceRand.normalized * speed;
        }
    }


    /* Ce code est buggé mais semblait pas mal... Ca sera plus simple de faire avec un deuxième collider qui ne collide qu'avec les murs, donc j'enlève

    //check with raycasting if our move (deplace) will make us collide with a wall
    private bool CanMove(Vector2 deplace, float range)
    {
        //cast in direction or our move to find nearest "Ground" stuff
        RaycastHit2D rh = Physics2D.Raycast(transform.position, deplace, 5*range, LayerMask.NameToLayer("Ground"));
        //get exact vector between the point of collision and the closest point of our collider
        //Vector2 dist = rh.point - (Vector2)GetComponent<PolygonCollider2D>().bounds.ClosestPoint(rh.point);
        Vector3 dist = rh.point - (Vector2)GetComponent<BoxCollider2D>().bounds.ClosestPoint(rh.point);

        //uncomment if need to see distance and rays in scene view
        Debug.DrawRay(transform.position, deplace*5*range, Color.blue, 1);
        //Debug.Log(dist.magnitude);
        //Debug.DrawRay(GetComponent<PolygonCollider2D>().bounds.ClosestPoint(rh.point), dist, Color.blue, 1);
        //Debug.DrawRay(GetComponent<BoxCollider2D>().bounds.ClosestPoint(rh.point), dist, Color.blue, 1);



        //return if movement if possible without hitting wall (and an additional range)
        return (dist.magnitude > deplace.magnitude + range);
    }

    */
}