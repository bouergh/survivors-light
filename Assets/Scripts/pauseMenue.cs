﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.Build.AssetBundle;
using UnityEngine;

public class pauseMenue : MonoBehaviour
{

    public static bool gamePaused = false;
    public GameObject pauseMenuUI;
    [SerializeField]
    private GameObject miniMap;

    // Use this for initialization
    void Start()
    {
        //pauseMenuUI = GameObject.FindGameObjectWithTag("Pause Menue");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!gamePaused)
                Pause();
            else
            {
                Resume();
            }
        }

    }

    private void Resume()
    {
        Cursor.visible = false;
        pauseMenuUI.SetActive(false);
        gamePaused = false;
        Time.timeScale = 1f;
        miniMap.SetActive(true);
    }

    private void Pause()
    {
        Cursor.visible = true;
        pauseMenuUI.SetActive(true);
        gamePaused = true;
        Time.timeScale = 0f;
        miniMap.SetActive(false);
    }
}
