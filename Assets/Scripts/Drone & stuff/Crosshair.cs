﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {
	
	private float zPos;

	// Use this for initialization
	void Start () {
		zPos = transform.position.z;
	}

    
	
	// Update is called once per frame
	void Update () {
		transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, zPos - Camera.main.transform.position.z));

	}
}
