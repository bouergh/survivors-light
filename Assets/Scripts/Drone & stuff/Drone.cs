﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : MonoBehaviour {

	public float droneSpeed = 1f; //is now a max distance (magnitude) to move in one fixed frame
	public float droneZpos = -1;

	//public GameObject pixelCamera;
	private GameObject Hero;
	private Light DroneLight;

	private Vector3 prevPos;
	private Vector3 diffPos;
	private Vector3 mousePos;
	private Vector3 newPos;

	private Collider2D collider;
	private bool stationnary = false;
	public bool fuckWalls = false;
	public bool powerAble = false;
	private bool wall;
	private ChangeLight changeLight;

	/* Respawn vars */
	private bool respawning = false;
	private bool respawn_step_1 = false;
	private bool respawn_step_2 = false;
	private bool respawn_step_3 = false;
	float original_intensity;
	public float decrement_intensity = 0.4f;
	public float respawnCooldown = 5f;


	// Use this for initialization
	void Start ()
	{
		GetComponent<Collider2D>().enabled = !fuckWalls; //if you click this in the editor, you won't consider wall a problem ! same as disabling the collider though
		//Vector3 pos = camera.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
		//could introduce bug if mouse not in the right place so we change (for initial position, after that we can take player head position or something)
		Vector3 pos = transform.position;
		collider = transform.GetChild(2).GetComponent<Collider2D>();
		changeLight = this.GetComponent<ChangeLight>();
		pos.z = droneZpos;
		transform.position = pos;

		//hides the cursor because it will seem like the drone doesn't go on it :/
		Cursor.visible = false;

		//finds the light of the drone
		DroneLight = GetComponentInChildren<Light>();

		//finds the hero in the scene
		Hero = GameObject.FindGameObjectWithTag("Player");

	   
	}

	/*void OnTriggerEnter2D(Collider2D other) {
		if(other.tag != "Ground"){
			Physics2D.IgnoreCollision (other, this.GetComponent<Collider2D> ());
		}
	}*/


	
	void FixedUpdate()
	{
		if (Input.GetButton("Fire3") && changeLight.getRotate() == 2)
		{
			collider.enabled = true;
		}
		else
		{
			collider.enabled = false;
		}
		if (respawning)
		{
			StartCoroutine(Respawn());
		}
		
		if (!stationnary) //sinon le drone pourra pas bouger pendant le cooldown (pas encore implém)
		{
			Move();
		}
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(1))
		{
			stationnary = !stationnary;
		}

		if (Input.GetMouseButtonDown(2))
		{
			if (!respawning)
				respawning = true;
		}
		
	}

	private void Move()
	{
		mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, droneZpos - Camera.main.transform.position.z));
		//mousePos = camera.GetComponent<Camera>().ViewportToWorldPoint(new Vector3(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height, droneZpos - camera.transform.position.z));
		//mousePos.z = droneZpos;
		//Debug.Log(mousePos);
		//GetComponent<Rigidbody2D>().velocity = diffPos / Time.deltaTime;
		//Mathf.Clamp(GetComponent<Rigidbody2D>().velocity.magnitude, 0f, droneSpeed);
		float step = droneSpeed * Time.deltaTime;
		//this.GetComponent<Rigidbody2D>().velocity = Vector3.MoveTowards(transform.position, mousePos, step);
		RaycastHit2D rc = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.right, step, 1 << LayerMask.NameToLayer("Ground"));
		
		this.GetComponent<Rigidbody2D>().MovePosition(Vector3.MoveTowards(transform.position, mousePos, step));
		
		
		//if (transform.position == mousePos) Debug.Log("drone a atteint le curseur...");
	}

	private IEnumerator Respawn()
	{
		// Everthing on false means we are entering in the respawn state
		if (!respawn_step_1 && !respawn_step_2 && !respawn_step_3)
		{
			// Backup previous light
			original_intensity = DroneLight.intensity;
			respawn_step_1 = true;
		}

		// Fading light out of the drone
		else if (respawn_step_1)
		{
			//Debug.Log("respawn phase 1");
			DroneLight.intensity -= decrement_intensity;
			if (DroneLight.intensity <= 0)
			{
				DroneLight.intensity = 0;

				// Teleporting at the end (keep the Z !)
				Vector3 telePos = new Vector3(Hero.transform.position.x, Hero.transform.position.y, transform.position.z);
				transform.position = telePos;
				
				// Next step
				respawn_step_1 = false;
				respawn_step_2 = true;
			}

			stationnary = false;
		}

		// Fading light in
		else if (respawn_step_2)
		{
			//Debug.Log("respawn phase 2");
			DroneLight.intensity += decrement_intensity;
			if (DroneLight.intensity >= original_intensity)
			{
				DroneLight.intensity = original_intensity;

				// Next step
				respawn_step_2 = false;
				respawn_step_3 = true;
			}
		}

		else if (respawn_step_3)
		{
			// Waiting for cooldown
			//Debug.Log("respawn phase 3");
			yield return new WaitForSeconds(respawnCooldown);
			// Reset the respawn ability
			//TODO
			if (true)
			{
				respawn_step_1 = false;
				respawn_step_2 = false;
				respawn_step_3 = false;
				respawning = false;
			}
		}
	}

	public Light GetLight()
	{
		return DroneLight;
	}

	
}
