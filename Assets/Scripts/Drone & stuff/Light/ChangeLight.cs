﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLight : MonoBehaviour {

    //public Color c1, c2, c3, c4;
    public int size;
    //private Color[] colorArray = new Color[5];
    [SerializeField]
    private CircleCollider2D powerCollider, rangeCollider;
    [SerializeField]
    private SpecialLight[] specialLights = new SpecialLight[5];
    [SerializeField]
    private GameObject selector;
    /*[SerializeField]
    private float r1, r2, r3, r4;
    private float[] rangeArray = new float[4];*/
    private int rotate = 0;
    //[SerializeField]
    //private Drone d;
    private Light DroneLight;

    private const int MAX_TURN = 4, MIN_TURN = 0;
    //public float range = 3f;

    // Use this for initialization
    void Start () {
        DroneLight = GetComponentInChildren<Light>();
       /*GameObject[] list = GameObject.FindGameObjectsWithTag("SpecialLight");
        for (int i = 0; i < 5; i++)
        {
            specialLights[i] = list[i].GetComponent<SpecialLight>();
        }*/
        //selector = GameObject.FindGameObjectWithTag("LightSelector");
        /*colorArray[0] = c1;
        colorArray[1] = c2;
        colorArray[2] = c3;
        colorArray[3] = c4;
        rangeArray[0] = r1;
        rangeArray[1] = r2;
        rangeArray[2] = r3;
        rangeArray[3] = r4;*/
        //powerCollider = transform.GetChild(2).GetComponent<CircleCollider2D>();
        //rangeCollider = this.GetComponents<CircleCollider2D>()[1];
    }

    public void changeLight(int i)
    {
        rotate = i;
        DroneLight.color = specialLights[i].activeColor;
    }

    public SpecialLight getLightArray(int i)
    {
        return specialLights[i];
    }

    public Light getDroneLight()
    {
        return DroneLight;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        var d = Input.GetAxis("Mouse ScrollWheel");
        if (d > 0f)
        {
            rotate++;
            if (rotate >= size || !specialLights[rotate].obtained)
                rotate = 0;
        }
        else if (d < 0f)
        {
            rotate--;
            if (rotate < 0)
                rotate = size-1;
            
        }
        //Debug.Log(rotate);
        while (!specialLights[rotate].obtained)
        {
            rotate--;
        }
        
        
        DroneLight.color = specialLights[rotate].activeColor;
        DroneLight.range = specialLights[rotate].range;
        powerCollider.radius = specialLights[rotate].range - 2;
        rangeCollider.radius = specialLights[rotate].range - 2;
        //selector.GetComponent<RectTransform>().position = specialLights[rotate].GetComponent<RectTransform>().position;
    }

    public int getRotate()
    {
        return rotate;
    }
}
