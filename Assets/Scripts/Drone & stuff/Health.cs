﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    
    public int health = 100;
	
	void heal(int hp)
    {
        health += hp;
    }

    void damage(int hp)
    {
        health -= hp;
    }

    bool isAlive()
    {
        return health >= 1;
    }
}
