﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject[] savePointList;
    public GameObject hero;
    public GameObject drone;
    public GameObject gun;

    private GameState current;

    // Use this for initialization
    void Awake () {
        current = Game.currentState;
        if (current != null)
        {
            if(Game.currentState.savePointIndex != -1)
            {
                hero.transform.position = new Vector3(savePointList[current.savePointIndex].transform.position.x, savePointList[current.savePointIndex].transform.position.y, hero.transform.position.z);
                if (gun && current.gotGun) gun.SetActive(true);
                if(drone && current.gotDrone)
                {
                    drone.SetActive(true);
                    drone.transform.position = new Vector3(hero.transform.position.x, hero.transform.position.y, drone.transform.position.z);
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
