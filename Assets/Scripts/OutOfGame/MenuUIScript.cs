﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUIScript : MonoBehaviour {

    public class Row{
        public Button save;
        public Text text;
        public Button load;
    }

    private bool canSave; //make this false if not accessed from the game (aka from the menu)
    
    public Button[] saveButtons;
    public Button[] loadButtons;
    public Text[] texts;
    

	// Use this for initialization
	void Start () {
        canSave = true;
        Debug.Log(Application.persistentDataPath);
        if (Game.current == null)
        {
            canSave = false;
            Game.current = new Game();
        }
        //initialize save button
        if (!canSave)
        {
            foreach(Button button in saveButtons)
            {
                button.gameObject.SetActive(false);
            }
        }

        SaveLoad.Load();
        //initialize text and load buttons
        for(int i =0; i < loadButtons.Length; i++)
        {
            if(Game.current.saves[i].savePointIndex == -1)
            {
                texts[i].text = "Save " + i + " empty.";
                loadButtons[i].GetComponentInChildren<Text>().text = "New";
            }
            else
            {
                texts[i].text = "Save from " + Game.current.saves[i].name;
            }
        }
	}

    private void OnEnable()
    {
        saveButtons[0].onClick.AddListener(() => Save(0));
        loadButtons[0].onClick.AddListener(() => Load(0));
        saveButtons[1].onClick.AddListener(() => Save(1));
        loadButtons[1].onClick.AddListener(() => Load(1));
        saveButtons[2].onClick.AddListener(() => Save(2));
        loadButtons[2].onClick.AddListener(() => Load(2));
    }





    public void Save(int n)// for slot number
    {
        Debug.Log("saving save number " + n);
        Game.current.saves[n] = Game.currentState;
        Game.current.saves[n].name = System.DateTime.UtcNow.ToString("yyyyMMddHHmmss");
        SaveLoad.Save();
        texts[n].text = "Save from " + Game.current.saves[n].name;
        loadButtons[n].GetComponentInChildren<Text>().text = "Load";
    }

    public void Load(int n) // for slot number
    {
        Debug.Log("loading save number " + n);
        SaveLoad.Load();
        //load the right scene and input the parameters !
        Game.currentState = Game.current.saves[n];
        if (Game.currentState.savePointIndex == -1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(Game.currentState.sceneIndex);
        }
        

    }

    /* old code for load, now we only need to take the data and go back to game when clicking here !
    public void Load(int n, Text text) // for slot number
    {
        Debug.Log("loading save number " + n);
        SaveLoad.Load();
        //int i = Game.current.saves[n].savePointIndex;
        //text.text = i.ToString();
        //Debug.Log(i);
        text.text = Game.current.saves[n].name;
    }
    */
}
