﻿// copypasta from
// https://gamedevelopment.tutsplus.com/tutorials/how-to-save-and-load-your-players-progress-in-unity--cms-20934
// and then adapted


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class Game
{

    public static Game current;
    public GameState[] saves;
    public static GameState currentState;

    public Game()
    {
        currentState = new GameState();
        saves = new GameState[3];//on va dire qu'on a 3 slots de sauvegarde pour l'instant
        saves[0] = new GameState();
        saves[1] = new GameState();
        saves[2] = new GameState();
    }
}

[System.Serializable]
public class GameState
{
    //need to put here everything we need to remember about the state of a game !
    public string name;
    public int sceneIndex;
    public int savePointIndex;
    public int actualHealth;
    public bool gotDrone = false;
    public bool gotGun = false;
    public int item1Number; //change them later for specific items, also state of the character increase in stuff and stats maybe
    public int item2Number;
    public int item3Number;

    public GameState()
    {
        name = System.DateTime.UtcNow.ToString("yyyyMMddHHmmss");
        savePointIndex = -1;    //indicates empty save
    }
}


//version modifie, juste 1 Game (qui contient plusieurs slots de GameState deja)
public static class SaveLoad
{
    public static Game savedGame;

    public static void Save()
    {
        if(Game.current == null)    //what we do if no game already exist (maybe change that and Load 'else' ?)
        {
            Game.current = new Game();
        }

        //then we can save normally this blank new game. We will update Game.current before saving from other scripts !
        savedGame = Game.current;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.sl");
        bf.Serialize(file, SaveLoad.savedGame);
        file.Close();

    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.sl"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.sl", FileMode.Open);
            SaveLoad.savedGame = (Game)bf.Deserialize(file);
            file.Close();
            Game.current = savedGame;
        }
        else
        {
            Save();
            //todo create new ? OR remove this function if no game saved, actually
        }
    }
}

//on a pas besoin d'une liste entière, car on a une array fixe dans Game déjà (3 slots)
/*
public static class SaveLoad
{
    
    public static List<Game> savedGames = new List<Game>();

    public static void Save()
    {
        savedGames.Add(Game.current);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.sl");
        bf.Serialize(file, SaveLoad.savedGames);
        file.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.sl"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.sl", FileMode.Open);
            SaveLoad.savedGames = (List<Game>)bf.Deserialize(file);
            file.Close();
        }
    }
}
*/