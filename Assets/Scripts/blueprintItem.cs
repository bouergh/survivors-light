﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blueprintItem : MonoBehaviour {
    public GameObject InventoryBP;

	// Use this for initialization
	void Start () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {

        GameObject other = collision.gameObject;
        if (other.tag == "Player")
        {
            other.GetComponent<Inventaire>().bluePrintList.Add(this.gameObject);
            InventoryBP.GetComponent<BluePrint>().activateBTN();
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
