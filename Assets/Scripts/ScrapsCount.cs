﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrapsCount : MonoBehaviour {

    //private int scrapCount;
	[SerializeField]
    ScrapsPlayer player;
	[SerializeField]
    private Text units, tens, hundreds;

	// Use this for initialization
	void Start ()
	{
		units = this.transform.GetChild(2).GetComponent<Text>();
		tens = this.transform.GetChild(1).GetComponent<Text>();
		hundreds = this.transform.GetChild(0).GetComponent<Text>();
		
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<ScrapsPlayer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (units != null && tens != null && hundreds != null)
		{
			units.text = player.units + "";
			tens.text = player.tens + "";
			hundreds.text = player.hundreds + "";
		}
	}
}
