﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour {

	public GameObject PrefabProjectile;
	private Transform projectileSpawn;

    [SerializeField]
    private float delay, cost, cooldown;
    public float energy;
    public const float MAX_ENERGY = 100;

	[SerializeField]
	private Slider powerBar;
    public float VitesseDeTir = 75f;
	public string projectileTag;

	private void Start()
	{
		projectileSpawn = transform.GetChild(0);
	}

	// Update is called once per frame
	void Update () {
        cooldown += Time.deltaTime;
        
		if (Input.GetButtonDown("Fire1") && !pauseMenue.gamePaused && energy - cost > 0)
		{
			AkSoundEngine.PostEvent ("Play_Massive_Laser_High" , gameObject);
			Vector2 directionDeTir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - projectileSpawn.position;
			float angle = (Mathf.Atan(directionDeTir.y / directionDeTir.x)) * Mathf.Rad2Deg;
			GameObject projectile = ObjectPooler.SharedInstance.GetPooledObject(projectileTag);
			if (projectile != null)
			{
				projectile.transform.position = projectileSpawn.position;
				projectile.transform.rotation = transform.rotation;
				projectile.SetActive(true);
			}
			projectile.GetComponent<Rigidbody2D>().velocity = directionDeTir.normalized * VitesseDeTir;
            energy -= cost;
		}
        if(cooldown >= delay)
        {
            if(energy < MAX_ENERGY)
                energy++;
            cooldown = 0;
           
        }

		powerBar.value = energy;
		/* ancienne version compliquee pour rien... mais peut-etre pas donc on la garde !
		if (Input.GetButtonDown("Fire1"))
		{
			// Tracer une ligne (ray) de la camera
			//Ray2D ray = Camera.main.ScreenPointToRay(Input.mousePosition) as Ray2D;
			Vector3 mousePosition = Input.mousePosition;

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			//Debug.Log (Input.mousePosition.x + " " + Input.mousePosition.y + " " + Input.mousePosition.z);
			// Detection du point entre la ligne et une surface
			if (hit.collider != null)
			{
				// Calcul de la direction du tir
				Vector2 pos2d = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
				Vector2 directionDeTir = (hit.point - pos2d);
				float angle = (Mathf.Atan (directionDeTir.y / directionDeTir.x)) * Mathf.Rad2Deg;
				Vector3 direction3D = new Vector3 (directionDeTir.x, directionDeTir.y, 0).normalized;
				// Instanciation d'une projectile a la position de cet objet + direction du tir pour eviter de mettre la projectile dans le joueur
				GameObject projectile = Instantiate(PrefabProjectile, this.gameObject.transform.position + direction3D, Quaternion.Euler(new Vector3(0,0,angle))) as GameObject;
				// Donner une vitesse a la projectile avec vitesse de tir et la direction normalisee
				projectile.GetComponent<Rigidbody2D>().velocity = directionDeTir.normalized * VitesseDeTir;
			}
		}
		*/

	}
}
