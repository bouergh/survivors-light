﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericProjectile : MonoBehaviour {

	public string hostileTag;
	public int damage;
    [SerializeField]
    private float push;
    private int x;

	// Use this for initialization
	void  OnEnable() {
		//gameObject.tag = "Projectile";
	}
	
	
	private void OnCollisionEnter2D(Collision2D coll)
	{
		
		Collider2D other = coll.collider;
		//Debug.Log(name + " collided with " + other.name);
		if (other.CompareTag(hostileTag) || other.CompareTag("Ground") || other.CompareTag("Door") || other.CompareTag("BossPlatform"))
		{
			if (other.tag == hostileTag)
			{
                if(other.transform.position.x < this.transform.position.x)
                {
                    x = -1;
                } else
                {
                    x = 1;
                }
				Damage damageScript = other.GetComponent<Damage>();
				if (damageScript != null) damageScript.TakeDamage(damage, x, push);
			}
			gameObject.SetActive(false);
		}
	}

}
