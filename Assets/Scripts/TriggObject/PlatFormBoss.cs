﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatFormBoss : TriggerObject {
    //heroPresent sert pour eviter le stuttering lorsque la plateforme se deplace avec le heros dessus
    public bool heroPresent = false;
    //enhaut sert a savoir que la plateforme est arrivee en haut et qu'on peut redescendre
    //terminer pour savoir si on a fait un cycle entier ou non (si non active et terminer, on ne remonte pas)
    public bool enhaut = false, terminer = true;
    //vitesse de deplacement et distance que peut atteindre la plateforme
    public float MaxDistance, speed,
        //distance parcourue par la plateforme
        distParcourue = 0;
    private GameObject hero;
    private float y;

    //will be activated by the boss, see his trigger
    public override void Active()
    {
        active = true;
    }

    public override void Desactive()
    {
        active = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log(gameObject.name + " collision enter");
        if (collision.gameObject.CompareTag("Player"))
        {
            heroPresent = true;
            hero = collision.gameObject;
        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            heroPresent = false;
        }
    }

    //deplace la plateforme dans la direction indique
    public void deplacePlateforme(int sens)
    {
        Vector3 deplace = new Vector3(0, speed * sens, 0);

        transform.Translate(deplace);

        if (heroPresent)
            hero.transform.Translate(deplace);  //en montee ca bloque les deplacements du joueur et en descente le joueur tombe a repetition sans ca !
                                                //corriger les anim !

        distParcourue += speed * sens;
        switch (sens)
        {
            case 1:
                if (distParcourue >= MaxDistance)
                    enhaut = true;  //on est arrive tout en haut, il faut redescendre
                break;
            case -1:
                if (distParcourue <= 0)
                {
                    enhaut = false; //on est revenu en bas, on pourra remonter
                    terminer = true;
                }
                break;
        }
    }

    //seplace la plateforme a chaque fixedUpdate
    private void FixedUpdate()
    {
        if (active) terminer = false;

        if (!terminer && !enhaut)  //on ne monte que si on est actif
        {
            deplacePlateforme(1);

        }
        else if (enhaut && !terminer)    //meme si on est plus actif, il faut redescendre
        {
            deplacePlateforme(-1);
        }
    }

    public GameObject getHero()
    {
        return hero;
    }

    public void setHero(GameObject hero)
    {
        this.hero = hero;
    }
}
