﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovingPlatforme : MovingPlatform
{

	// Use this for initialization
	void Start () {
        active = true;
        terminer = false;
    }

    
    private void FixedUpdate()
    {
        if (!terminer && active)
        {
            deplacePlateforme(1);

        }
        else if (terminer && active)
        {
            deplacePlateforme(-1);
        }
        else if (terminer && !active)
        {
            terminer = false;
            active = true;
        }
    }
}
