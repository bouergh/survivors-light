﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : TriggerObject {

    private float size, deplaceTotal = 0f;
    public float deplacement;
    private bool open = false, opened = false;
    public float closeTime = 5f;
    private float cTimer = 0f;
    // Use this for initialization
    void Start () {

        size = GetComponent<SpriteRenderer>().bounds.size.y;
    }

    public override void Active()
    {
        open = true;
    }

    public override void Desactive()
    {
        open = false;
    }

    void FixedUpdate()
    {
        if (open && !opened)   //door opens, should be done very quick, and don't move more than its size
        {
            Vector3 mvt = new Vector3(0, deplacement, 0);
            transform.Translate(mvt);
            deplaceTotal += deplacement;
            if (deplaceTotal >= size)
            {
                opened = true;
            }

        }
        if (!open)  //door closes if time's up, very quick, not more than its size
        {
            cTimer += Time.fixedDeltaTime;
            if (cTimer >= closeTime)
            {
                Vector3 mvt = new Vector3(0, -deplacement, 0);
                transform.Translate(mvt);
                deplaceTotal -= deplacement;
                if (deplaceTotal <= 0f)
                {
                    opened = false;
                    cTimer = 0f;
                }
            }

        }
    }
}

