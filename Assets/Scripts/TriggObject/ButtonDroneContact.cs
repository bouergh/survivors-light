﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonDroneContact : MonoBehaviour {

    public TriggerObject trigObj;

    void Start()
    {
        
    }
    void FixedUpdate()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Drone"))
        {
            trigObj.Active();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Drone"))
        {
            trigObj.Desactive();
        }
    }
}
