﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerObject : MonoBehaviour {

    public bool active;

    public abstract void Active();

    public abstract void Desactive();

    
}
