﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonLight : MonoBehaviour {

    public TriggerObject trigObj;
    private GameObject drone;
    [SerializeField]
    private bool keyReleased = true;
    private Light droneLight;
    public int couleur;
    private float range;
    private bool activated = false;
    [SerializeField]
    private float defaultIntensity;
    [SerializeField]
    private float newIntensity;
    
    // Use this for initialization
    void Start()
    {
        drone = GameObject.FindGameObjectWithTag("Drone");
        droneLight = drone.GetComponentInChildren<Light>();
        defaultIntensity = droneLight.intensity;
        range = drone.GetComponentInChildren<Light>().range;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Drone")&& drone.GetComponent<ChangeLight>().getRotate() == 1)
        {
            activated = true;
            droneLight.intensity = newIntensity;

        }
        else
        {
            activated = false;
            droneLight.intensity = defaultIntensity;
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Drone")
        {
            activated = false;
            droneLight.intensity = defaultIntensity;

        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetButtonUp("Fire3"))
        {
            keyReleased = true;
        }
        
        int rotate = drone.GetComponent<ChangeLight>().getRotate();
        if (Input.GetButton("Fire3") && activated && !trigObj.active && keyReleased)
        {
            trigObj.Active();
            keyReleased = false;
        }
        else if (trigObj.active && Input.GetButton("Fire3") && keyReleased)
        {
            trigObj.Desactive();
            keyReleased = false;
        }

    }
}
