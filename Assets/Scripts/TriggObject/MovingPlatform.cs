﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : TriggerObject
{
    public bool terminer = false, heroPresent = false;
    //vitesse de deplacement et distance que peut atteindre la platteforme
    public float MaxDistance, speed,
        //distance parcourue par la plateforme
        distParcourue = 0;
    private GameObject hero;
    private float y;


    // Use this for initialization
    void Start()
    {
    }
    public override void Active()
    {
        active = true;
    }

    public override void Desactive()
    {
        //active = false;
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        heroPresent = true;
        hero = collision.gameObject;
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        heroPresent = false;
        //terminer = true;
    }

    //deplace la plateforme dans la direction indique
    public void deplacePlateforme(int sens)
    {
        Vector3 deplace = new Vector3(speed * sens, 0, 0);
        this.transform.Translate(deplace);
        if (heroPresent)
            hero.transform.Translate(deplace);
        distParcourue += speed * sens;
        switch (sens)
        {
            case 1:
                if (distParcourue >= MaxDistance)
                    terminer = true;
                break;
            case -1:
                if (distParcourue <= 0)
                    active = false;
                break;
        }
    }

    //seplace la plateforme a chaque fixedUpdate
    private void FixedUpdate()
    {
        if (active && !terminer)
        {
            deplacePlateforme(1);

        }
        else if (terminer && active && !heroPresent)
        {
            deplacePlateforme(-1);
        }
        else if (terminer && !active)
        {
            terminer = false;
        }
    }

    public GameObject getHero()
    {
        return hero;
    }

    public void setHero(GameObject hero)
    {
        this.hero = hero;
    }
}
