﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class SpecialLight : MonoBehaviour {

    public Image image;
    public Color activeColor;
    public float range;
	public string description;
	public bool obtained;


    // Use this for initialization
	private void OnEnable()
	{
		image = this.GetComponent<Image>();
		activeColor = image.color;
	}
	
	
	// Update is called once per frame
	void Update () {
		
	}
}
