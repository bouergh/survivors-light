﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapsPlayer : MonoBehaviour
{

	public int units, tens, hundreds;

	// Use this for initialization
	void Start () {
		
	}

	public void addScrap()
	{
		units++;
		if (units >= 10)
		{
			tens++;
			units = 0;
		}

		if (tens >= 10)
		{
			hundreds++;
			tens = 0;
		}

		if (hundreds >= 10)
		{
			units = 9;
			tens = 9;
			hundreds = 9;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
