﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMortel : MonoBehaviour {

    [SerializeField]
    private int damage; //si le truc est vraiment mortel, il n'y a qu'à mettre une très grande valeur ici !
	//public int pushForce;
	//private int x;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            //mort du perso, ou perte de point de vie... Placeholder !
            Debug.Log("Le personnage " + other.name + " prend "+damage+" dégâts car il vient de collide avec "+gameObject.name+" !");
            Damage damageScript = other.GetComponent<Damage>();

            /* Partie de Michael, obsolete ? cf Repousse.cs
			if ((this.transform.position.x - other.transform.position.x) > 0)
				x = -1;
			else
				x = 1;
                */

            if (damageScript != null) damageScript.TakeDamage(damage);
            
        }

        //à voir si un piège peut aussi affecter les ennemis ?
    }
}
