﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrap : MonoBehaviour
{
	[SerializeField]
	private int value;

	// Use this for initialization

	private void OnTriggerEnter2D(Collider2D collision)
	{
		
		GameObject other = collision.gameObject;
		if (other.tag == "Player")
		{
			ScrapsPlayer sP = other.GetComponent<ScrapsPlayer>();
		
			sP.addScrap();
			gameObject.SetActive(false);
		}else if (other.tag == "GroundCheck")
		{
			ScrapsPlayer sP = other.transform.parent.gameObject.GetComponent<ScrapsPlayer>();
			sP.addScrap();
			gameObject.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
