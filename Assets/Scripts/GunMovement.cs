﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunMovement : MonoBehaviour {

    //Animator anim;
    private Animator anim;
    public Transform crosshair;
    
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
        //there are 5 frames in animation, and normalized time is between 0 and 1f
        //anim.Play("Gun0", -1, 0.6f);    //0 is current animation, -1 is default, next thing is normalized time frame

        //implementation mille fois plus intelligente que la precedente avec les angles et les worldPosition
        Vector2 localCrosshair = transform.InverseTransformPoint(crosshair.position);   
        //Debug.Log(localCrosshair.y);
        if (localCrosshair.y < -2f * localCrosshair.x)
        {
            anim.Play(0, -1, 0.01f);
        }
        else if (localCrosshair.y < -localCrosshair.x / 2f)
        {
            anim.Play(0, -1, 0.25f);
        }
        else if (localCrosshair.y < localCrosshair.x / 2f)
        {
            anim.Play(0, -1, 0.50f);
        }
        else if (localCrosshair.y < 2f * localCrosshair.x)
        {
            anim.Play(0, -1, 0.75f);
        }
        else
        {
            anim.Play(0, -1, 0.90f);
        }
        // les cas x<0 sont geres par l'utilisation du InverseTransformPoint et cela permet d'appliquer la methode a des objets qui ne sont pas toujours avec des rotations nulles (sur Unity)
    }
}
