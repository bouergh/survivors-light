﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repousse : MonoBehaviour {

    [SerializeField]    //quel type d'objet (par tag) cet objet repousse-t-il ?
    private string tagCible = "Player";
    [SerializeField]    //avec quelle force le repousse-t-il ? Force négative pour attirer ? Appel d'une fonction sur la victime peut être sinon ça va être le bordel...
    private float force = 10f;
    [SerializeField]    //pendant quel temps
    private float time = 1f;

    private void OnCollisionEnter2D(Collision2D coll)
    {
        Collider2D collision = coll.otherCollider;
        if(collision.tag == tagCible)
        {
            Vector2 direction = (collision.transform.position - transform.position).normalized;
            StartCoroutine(collision.GetComponent<Damage>().Repousse(force * direction, time));
        }
    }
    //l'un ou l'autre ? Ca pourrait être utile pour des trucs qui sont seulement trigger !
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == tagCible)
        {
            Vector2 direction = (collision.transform.position - transform.position).normalized;
            StartCoroutine(collision.GetComponent<Damage>().Repousse(force * direction, time));
        }
    }
}
