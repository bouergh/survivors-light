﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour {

	public float speed, forceJump;
    public float EPSILON = 0.01f;   //reduce this so player can maintain it's max height in jump longer
    public float UPSILON = 0.01f;   //distance au plafond pour saut des joueurs (minime !)
    private bool grounded = true, slope = false, coll = false, run = false;
    public float maxJumpSpeed, minJumpSpeed;
    public float jumpHeight;
    private bool canJump, hasJumped;
    private float height = 0;   // internal value so shouldn't be public
    public bool beingPushed = false, ladder = false;
    private float x, lastX, y;
    public float jumpSpeed;
    private float posXInit, posXActu;
    private Vector2 veloc;
    private Transform groundCheck, slopeChk1, slopeChk2;
    private float origGravity;
    private Animator anim;
    private Collider2D collider;

    [SerializeField]
    private bool facingRight = true;

    [SerializeField]
    private bool isJumping;

	// Use this for initialization
	void Start () {
        isJumping = false;
        canJump = true;
        hasJumped = false;
        origGravity = GetComponent<Rigidbody2D>().gravityScale;
        anim = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void FixedUpdate () {

        if (!beingPushed /*&& !ladder*/) //check if being pushed (when colliding with trap you can't move for some time)
        {
            
            x = Input.GetAxis("Horizontal");
            float translate = x * speed;
            //to avoid player going "inside" walls (and exploiting jumping system to walljump for example) we check for surfaces on left or right of collider
            RaycastHit2D rc = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.right, translate, 1 << LayerMask.NameToLayer("Ground"));
            if (!rc)    //uses BoxCast to raycast the future translation and cancel it if too much
            {
                this.transform.Translate(x * speed, 0, 0);
            }/*
            else
            {
                Debug.Log("wall blocking, can't move further, time:" + Time.fixedTime);
            }*/
            anim.SetBool("isWalking", translate != 0f); //sets walking anim float according to moving or not...

        }
        if (ladder)
        {
            y = Input.GetAxis("Vertical");
            float translate = y * speed;
            //to avoid player going "inside" walls (and exploiting jumping system to walljump for example) we check for surfaces on left or right of collider
            RaycastHit2D rc = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.right, translate, 1 << LayerMask.NameToLayer("Ground"));
            if (!rc)    //uses BoxCast to raycast the future translation and cancel it if too much
            {
                this.transform.Translate(0, y * speed, 0);
            }
            canJump = true;
            GetComponent<Rigidbody2D>().gravityScale = 0;

        }
        
        


        if (Input.GetButton("Jump") && !isJumping && canJump) // input -> dans Update
        {
            StartCoroutine(Jump());
        }

        else if (isGrounded() && !isJumping)
        {
            canJump = true;
        }
        
        SetAnim();

        //call to turn the character around. could fuck things like shadows or collisions because of normal vector change ! CAREFUL
        if((facingRight && (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).x < 0) || (!facingRight && (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).x > 0))
        {
            Flip();
        }
        
    }

    private void Update()
    {

        
    }
    
    IEnumerator Jump()
    {
        canJump = false;
        isJumping = true;
        GetComponent<Rigidbody2D>().gravityScale = 0;
        anim.SetTrigger("Jump");
        anim.SetBool("isGrounded", false);
        while (height < jumpHeight && Input.GetButton("Jump"))
        {
            float heightRatio = height / jumpHeight;
            float trans = Mathf.Lerp(maxJumpSpeed, minJumpSpeed, heightRatio);
            
            //to avoid player going "inside" walls (and failing the other wall check...) we check for surfaces on top of collider
            RaycastHit2D rc = Physics2D.BoxCast(transform.position, collider.bounds.extents, 0f, Vector2.up, trans, 1 << LayerMask.NameToLayer("Ground"));
            if (rc)    //uses BoxCast to raycast the future translation and reduce it if too much
            {
                float distance = (rc.point - (Vector2)collider.bounds.center).magnitude - collider.bounds.extents.y;
                //Debug.Log("Actual translation is "+trans+" and distance to collider is "+distance);
                trans = Mathf.Min(trans-UPSILON,distance-UPSILON,0f);    //uses epsilon for now for min distance to upper wall
                //height = jumpHeight; //and stops jump
            }
            if (trans <= EPSILON)   //so if distance to height is inferior to epsilon, we will stop jumping
                height = jumpHeight;
            transform.Translate(0, trans, 0);
            height += trans;
            yield return new WaitForFixedUpdate();
        }
        isJumping = false;
        height = 0;
        GetComponent<Rigidbody2D>().gravityScale = origGravity;
            
    }

    private bool isGrounded()
    {
        //check ground with collider of child. use a overlapcircle based on it instead (too much walljump with the box)
        Collider2D coll = transform.Find("GroundCheck").GetComponent<Collider2D>(); //based on the child called "GroundCheck" which is a collider
        //RaycastHit2D rc = Physics2D.BoxCast(transform.position, coll.bounds.size, 0f, Vector2.down, (coll.transform.position-transform.position).magnitude, 1 << LayerMask.NameToLayer("Ground"));
        int mask = LayerMask.GetMask("Ground", "Scraps");
        Collider2D rc = Physics2D.OverlapCircle(transform.Find("GroundCheck").position, coll.bounds.extents.x, mask /*1 << LayerMask.NameToLayer("Ground")*/);
        return rc;  //no collision -> rc is null -> false bool
    }

    private void SetAnim()  //checking velocity to set triggers and bools in animator
    {
        if(GetComponent<Rigidbody2D>().velocity.y < 0f)
        {
            anim.SetTrigger("Fall");
            anim.SetBool("isGrounded", false); //fall without jump won't work if we don't set false manually here
        }
        else
        {
            if (isGrounded())
            {
                anim.SetBool("isGrounded", true);
            }
        }
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}

