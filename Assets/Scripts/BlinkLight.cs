﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkLight : MonoBehaviour {

    private new Light light;
    private float timer;
    public int startIntensity;
    public float time1;
    public float time2;
    public float intensity1;
    public float intensity2;

	// Use this for initialization
	void Awake () {
        light = GetComponent<Light>();
        switch (startIntensity)
        {
            case 1:
                light.intensity = intensity1;
                timer = time1;
                break;
            case 2:
                light.intensity = intensity2;
                timer = time2;
                break;
            default:
                light.intensity = intensity1;
                timer = time1;
                break;
        }

	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;
        if (timer <= 0f) Toggle();
		
	}

    private void Toggle()
    {
        if (light.intensity == intensity1)
        {
            light.intensity = intensity2;
            timer = time2;
        }
        else if (light.intensity == intensity2)
        {
            light.intensity = intensity1;
            timer = time1;
        }
    }



}
