﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunPowerLabel : MonoBehaviour {

    private Shoot gun;
    private float energy;
    private Text text;

	// Use this for initialization
	private void OnEnable()
	{
		gun = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Shoot>();
		energy = gun.energy;
		//text = this.GetComponent<Text>();
	}
        
	
	// Update is called once per frame
	void Update () {
        energy = gun.energy;
        //text.text = "Gun Power : " + energy + "%";
	}
}
