﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTrigger : MonoBehaviour {

    private GameObject light;


	// Use this for initialization
	void Start () {
        light = transform.GetChild(0).gameObject;
        light.SetActive(false);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("calice");
        if (collision.CompareTag("Player"))
            light.SetActive(true);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            light.SetActive(false);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
