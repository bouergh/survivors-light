﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour {

	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
    public float yOffset;
    public Transform target;

    private void Start()
    {
        //transform.position = new Vector3(transform.position.x, target.position.y + yOffset, transform.position.z);
    }

    // Update is called once per frame
    void FixedUpdate () 
	{
		if (target)
		{
			Vector3 point = this.GetComponent<Camera>().WorldToViewportPoint(target.position);
            //point = new Vector3(point.x, point.y + yOffset, point.z);
			Vector3 delta = target.position - this.GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
            destination.y += yOffset;
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            
		}
        
    }
}