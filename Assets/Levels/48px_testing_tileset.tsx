<?xml version="1.0" encoding="UTF-8"?>
<tileset name="48px_test-tileset" tilewidth="48" tileheight="48" tilecount="100" columns="10">
 <image source="48px_test-tileset.png" width="480" height="480"/>
 <tile id="23">
  <objectgroup draworder="index">
   <object id="21" x="0" y="0" width="48.0909" height="48.0909"/>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index">
   <object id="1" x="-1" y="0" width="48" height="49"/>
  </objectgroup>
 </tile>
</tileset>
