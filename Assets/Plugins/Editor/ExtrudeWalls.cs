﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.Tilemaps;



public class MeshExtruder : EditorWindow
{
    [MenuItem("Tools/Mesh Extruder")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(MeshExtruder));
    }

    
    float zExtrusion = 0f;
    Mesh meshToExtrude;
    string newName;
    Mesh newMesh;

    void OnGUI()
    {
        GUILayout.Label("Settings", EditorStyles.boldLabel);
        zExtrusion = EditorGUILayout.FloatField("Z Extrusion Size", zExtrusion);
        meshToExtrude = (Mesh) EditorGUILayout.ObjectField("Mesh to extrude", meshToExtrude, typeof(Mesh), false);
        //can generate name from original mesh name
        if (GUILayout.Button("Auto name"))
            newName = "extruded_" + meshToExtrude.name;
        //can specify an name manually
        newName = EditorGUILayout.TextField("Name for the extruded mesh",newName);
        //when clicked, will call the MeshExtrusion.cs for extrusion and store the new mesh in the assets
        if (GUILayout.Button("Generate extruded Mesh"))
        {
            newMesh = new Mesh();
            ExtrudeMesh(meshToExtrude, newMesh, zExtrusion);
            AssetDatabase.CreateAsset(newMesh, "Assets/Mesh/" + newName + ".asset");
        }
            

    }

    //adapted call of ExtrudeMesh from MeshExtrusion.cs for simple z translation (instead of Matrix4x4 transformation that I had actually forgot before today)
    public void ExtrudeMesh(Mesh originalMesh, Mesh newMesh, float zSize)
    {
        Matrix4x4[] matrix =
            {
                new Matrix4x4(
                    new Vector4(1, 0, 0, 0),
                    new Vector4(0, 1, 0, 0),
                    new Vector4(0, 0, 1, 0),
                    new Vector4(0, 0, 0, 1)),   //base of transfo is always a 1-diag

                new Matrix4x4(
                    new Vector4(1, 0, 0, 0),
                    new Vector4(0, 1, 0, 0),
                    new Vector4(0, 0, 1, 0),
                    new Vector4(0, 0, -zSize, 1))//putting zSize here will make a simple translation of it on z axis
            };
        MeshExtrusion.ExtrudeMesh(originalMesh, newMesh, matrix, false);
    }

    // Add a menu item called "Extrude it !" to a Mesh Filter's context menu.
    /*
    [MenuItem("CONTEXT/Mesh Filter/Extrude it")]
    static void DoubleMass(MenuCommand command)
    {
        Rigidbody body = (Rigidbody)command.context;
        body.mass = body.mass * 2;
        Debug.Log("Doubled Rigidbody's Mass to " + body.mass + " from Context Menu.");
    }
    */
    
}

[CustomEditor(typeof(MeshFilter))]
public class InspectorMeshExtrusion : Editor
{

    float zExtrusion;
    Mesh meshToExtrude;
    string newName;
    Mesh newMesh;




    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Generate extruded Mesh"))
        {
            //init
            zExtrusion = 60f;   //arbitrary value to have such thick walls
            newMesh = new Mesh();
            meshToExtrude = Selection.activeGameObject.GetComponent<MeshFilter>().sharedMesh;
            newName = "extruded_"+meshToExtrude.name+".asset";
            //update the new mesh and store it in resources
            ExtrudeMesh(meshToExtrude, newMesh, zExtrusion);
            AssetDatabase.CreateAsset(newMesh, "Assets/Mesh/" + newName);
            //change the mesh on the GameObject + move its transform
            Selection.activeGameObject.GetComponent<MeshFilter>().sharedMesh = AssetDatabase.LoadAssetAtPath<Mesh>("Assets/Mesh/" + newName);
            Selection.activeGameObject.transform.position += new Vector3(0f,0f,50f);    //arbitrary value from the 60f, to place said walls right
        }
    }

    //double from the other thing, might delete the whole other thing
    //adapted call of ExtrudeMesh from MeshExtrusion.cs for simple z translation (instead of Matrix4x4 transformation that I had actually forgot before today)
    public void ExtrudeMesh(Mesh originalMesh, Mesh newMesh, float zSize)
    {
        Matrix4x4[] matrix =
            {
                new Matrix4x4(
                    new Vector4(1, 0, 0, 0),
                    new Vector4(0, 1, 0, 0),
                    new Vector4(0, 0, 1, 0),
                    new Vector4(0, 0, 0, 1)),   //base of transfo is always a 1-diag

                new Matrix4x4(
                    new Vector4(1, 0, 0, 0),
                    new Vector4(0, 1, 0, 0),
                    new Vector4(0, 0, 1, 0),
                    new Vector4(0, 0, -zSize, 1))//putting zSize here will make a simple translation of it on z axis
            };
        MeshExtrusion.ExtrudeMesh(originalMesh, newMesh, matrix, false);
    }

}

//new version for the tilemap object !
[CustomEditor(typeof(TilemapRenderer))]
public class InspectorTilemapExtrusion : Editor
{

    float zExtrusion;
    Mesh meshToExtrude;
    string newName;
    Mesh newMesh;

    //this time in two times : first create a quad from the tilemap or the tilemap collider or the composite collider
    //then use existing code to generate the extruded mesh as usual

    //the script to create a child which is a quad mesh
    public void CreateQuad(CompositeCollider2D coll)
    {
        //get number number of paths and create a child quad for each
        int nQuad = coll.pathCount;
        for(int i = 0; i < nQuad; i++)
        {
            //instantiate an child with good tag, layer and components
            GameObject go = (GameObject) Instantiate(Resources.Load("EmptyShadowMesh", typeof(GameObject)));
            go.name = "Quad" + i;
            go.transform.parent = coll.transform;
            //create quad mesh from collider path
            Mesh mesh = new Mesh();
                //path to vertices
            Vector2[] vertices2D = new Vector2[coll.GetPathPointCount(i)];
            coll.GetPath(i, vertices2D);
                //2D to 3D
            Vector3[] vertices = new Vector3[vertices2D.Length];
            for (int j = 0; j < vertices.Length; j++)
            {
                vertices[j] = new Vector3(vertices2D[j].x, vertices2D[j].y, 0);
            }
                //vertices to triangles
            Triangulator tr = new Triangulator(vertices2D); //triangulator code CC-SA from https://wiki.unity3d.com/index.php?title=Triangulator
            int[] indices = tr.Triangulate();
                //assign
            mesh.vertices = vertices;
            mesh.triangles = indices;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            //extrude said mesh
            Mesh exMesh = new Mesh();
            zExtrusion = 60f;
            ExtrudeMesh(mesh, exMesh, zExtrusion);
            //attach the mesh 
            go.GetComponent<MeshFilter>().mesh = exMesh;
            //move the object AND VOILA
            //go.transform.position += new Vector3(0f, 0f, 50f);

            //TestMesh(go);
        }
    }

    //a changer
    public void DoTheFlop()
    {
        //init
        zExtrusion = 60f;   //arbitrary value to have such thick walls
        newMesh = new Mesh();
        meshToExtrude = Selection.activeGameObject.GetComponent<MeshFilter>().sharedMesh;
        newName = "extruded_" + meshToExtrude.name + ".asset";
        //update the new mesh and store it in resources
        ExtrudeMesh(meshToExtrude, newMesh, zExtrusion);
        AssetDatabase.CreateAsset(newMesh, "Assets/Mesh/" + newName);
        //change the mesh on the GameObject + move its transform
        Selection.activeGameObject.GetComponent<MeshFilter>().sharedMesh = AssetDatabase.LoadAssetAtPath<Mesh>("Assets/Mesh/" + newName);
        Selection.activeGameObject.transform.position += new Vector3(0f, 0f, 50f);    //arbitrary value from the 60f, to place said walls right
    }
    

    public void TestMesh(GameObject go)
    {

        float width = 12f;
        float height = 10f;

        MeshFilter mf  = go.GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        mf.mesh = mesh;

        Vector2[] vertices2D  = new Vector2[4];

        vertices2D[0] = new Vector2(0, 0);
        vertices2D[1] = new Vector2(width, 0);
        vertices2D[2] = new Vector2(0, height);
        vertices2D[3] = new Vector2(width, height);

        

        int[] tri = new int[6];

        tri[0] = 0;
        tri[1] = 2;
        tri[2] = 1;

        tri[3] = 2;
        tri[4] = 3;
        tri[5] = 1;

        //2D to 3D
        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int j = 0; j < vertices.Length; j++)
        {
            vertices[j] = new Vector3(vertices2D[j].x, vertices2D[j].y, 0);
        }
        //vertices to triangles
        Triangulator tr = new Triangulator(vertices2D); //triangulator code CC-SA from https://wiki.unity3d.com/index.php?title=Triangulator
        int[] indices = tr.Triangulate();

        mesh.vertices = vertices;

        mesh.triangles = tri;

        Vector3[] normals = new Vector3[4];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;

        mesh.normals = normals;

        Vector2[] uv = new Vector2[4];

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);

        mesh.uv = uv;
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Generate extruded Mesh"))
        {
            CreateQuad(Selection.activeGameObject.GetComponent<CompositeCollider2D>());
        }
    }

    //double from the other thing, might delete the whole other thing
    //adapted call of ExtrudeMesh from MeshExtrusion.cs for simple z translation (instead of Matrix4x4 transformation that I had actually forgot before today)
    public void ExtrudeMesh(Mesh originalMesh, Mesh newMesh, float zSize)
    {
        Matrix4x4[] matrix =
            {
            new Matrix4x4(
                new Vector4(1, 0, 0, 0),
                new Vector4(0, 1, 0, 0),
                new Vector4(0, 0, 1, 0),
                new Vector4(0, 0, 0, 1)),   //base of transfo is always a 1-diag

            new Matrix4x4(
                new Vector4(1, 0, 0, 0),
                new Vector4(0, 1, 0, 0),
                new Vector4(0, 0, 1, 0),
                new Vector4(0, 0, -zSize, 1))//putting zSize here will make a simple translation of it on z axis
        };
        MeshExtrusion.ExtrudeMesh(originalMesh, newMesh, matrix, false);
    }
}

