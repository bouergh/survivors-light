﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValueColor : MonoBehaviour
{

	[SerializeField]
	private Image bar;

	private Slider _slider;
	
	[SerializeField]
	private int value;

	// Use this for initialization
	void Start ()
	{
		_slider = this.GetComponent<Slider>();
	}

	public void ChangeColor()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		value = (int)_slider.value;
	}
}
