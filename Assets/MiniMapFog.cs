﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MiniMapFog : MonoBehaviour
{

	private TilemapRenderer _tilemapRenderer;

	// Use this for initialization
	void Start ()
	{
		_tilemapRenderer = this.GetComponent<TilemapRenderer>();
		_tilemapRenderer.enabled = false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		this._tilemapRenderer.enabled = true;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
