﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMiniMap : MonoBehaviour
{

	[SerializeField]
	private Drone _drone;
	private Color color;
	private SpriteRenderer _spriteRenderer;

	// Use this for initialization
	private void OnEnable()
	{
		_drone = this.transform.parent.gameObject.GetComponent<Drone>();
		color = _drone.GetComponentInChildren<Light>().color;
		_spriteRenderer = this.GetComponent<SpriteRenderer>();
	}
	

	// Update is called once per frame
	void Update () {
		color = _drone.GetLight().color;
		_spriteRenderer.color = color;
	}
}
