﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

	private GameObject spawnedObject;

	// Use this for initialization
	void Start ()
	{
		spawnedObject = this.transform.GetChild(0).gameObject;

	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		spawnedObject.SetActive(true);
	}

	/*private void OnTriggerExit2D(Collider2D other)
	{
		spawnedObject.SetActive(false);
	}*/

	// Update is called once per frame
	void Update () {
		
	}
}
