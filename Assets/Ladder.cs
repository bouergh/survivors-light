﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour {

    Mouvement mvt;

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        if(other.tag == "Player" /*&& Input.GetAxis("Vertical") > 0*/)
        {
            mvt = other.GetComponent<Mouvement>();
            mvt.ladder = true;
            other.GetComponent<Rigidbody2D>().velocity = new Vector2 (0,0);
        }
        
    }

    /*private void OnTriggerStay2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        if (other.tag == "Player"/*&& Input.GetAxis("Vertical") > 0*//*)
        {
            mvt = other.GetComponent<Mouvement>();
            mvt.ladder = true;
            //other.GetComponent<Rigidbody2D>().gravityScale = 0;
        }
        
    }*/


    private void OnTriggerExit2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        if (other.tag == "Player")
        {
            mvt = other.GetComponent<Mouvement>();
            mvt.ladder = false;
            other.GetComponent<Rigidbody2D>().gravityScale = 3;
        }
        
    }
    // Update is called once per frame
    void Update () {
		
	}
}
