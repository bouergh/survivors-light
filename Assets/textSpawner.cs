﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textSpawner : MonoBehaviour
{

	[SerializeField]
	private string message;

	[SerializeField]
	private float timer, delay;

	[SerializeField]
	private Text text;

	[SerializeField]
	private bool timerStart = false, triggerByColl;
	
	// Use this for initialization
	void Start () {
		
	}

	public void activateTutorial()
	{
		text.transform.parent.gameObject.SetActive(true);
		text.text = message;
		timerStart = true;
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (triggerByColl && other.tag == "Player")
		{
			activateTutorial();
			Debug.Log("tuto");
		}
	}

	// Update is called once per frame
	void Update () {
		if (timerStart)
		{
			timer += Time.deltaTime;
			if (timer >= delay)
			{
				timer = 0;
				timerStart = false;
				text.transform.parent.gameObject.SetActive(false);
				this.gameObject.SetActive(false);
			}
		}
	}
}
