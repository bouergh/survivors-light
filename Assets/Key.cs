﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Key : MonoBehaviour
{

	[SerializeField]
	private Door Door;
	[SerializeField]
	public bool got = false;

	// Use this for initialization
	void Start () {
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Door.Active();
		got = true;
		this.gameObject.SetActive(false);
		
	}

	// Update is called once per frame
	void Update () {
		
	}
}
