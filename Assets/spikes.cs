﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spikes : MonoBehaviour
{

	[SerializeField]
	private int x, damage;
	[SerializeField]
	private float pushForce;
	
	// Use this for initialization
	void Start () {
		
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		GameObject collObj = other.gameObject;
		if (collObj.tag == "Player")
		{
			if (other.gameObject.transform.position.x < this.transform.position.x)
			{
				x = -1;
			}
			else
			{
				x = 1;
			}
			Damage damageScript = collObj.GetComponent<Damage>();
			if (damageScript != null) damageScript.TakeDamage(damage, x, pushForce);
		}
	}

	private void OnCollisionStay2D(Collision2D other)
	{
		GameObject collObj = other.gameObject;
		if (collObj.tag == "Player")
		{
			if (other.gameObject.transform.position.x < this.transform.position.x)
			{
				x = -1;
			}
			else
			{
				x = 1;
			}
			Damage damageScript = collObj.GetComponent<Damage>();
			if (damageScript != null) damageScript.TakeDamage(damage, x, pushForce);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
