﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentLight : MonoBehaviour
{

	[SerializeField] private Image _image;
	[SerializeField] private ChangeLight _drone;

	// Use this for initialization
	void OnEnable ()
	{
		_image = this.GetComponent<Image>();
		
	}

	public void changeColor(Color color)
	{
		_image.color = color;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_image.color = _drone.getDroneLight().color;
	}
}
